# Ejemplo RSA

## Manual

### Cifrado

#### Semiautomático

1. Elegimos dos números primos. $` p=11 `$ y $` q=17 `$.
1. Luego $` n=11 \cdot 17 = 187 `$
1. Su totiente $` \varphi(n) = 10 \cdot 16 = 160 `$
1. Elegimos dos claves $` d `$ y $` e `$ tales que $` d \cdot e \equiv 1 \quad (mod \; 160) `$

```bash
$  ./pairs 160
```

1. Tomamos por ejemplo 57 (39h) y 73 (49h).
1. Pasamos una cadena a hexadecimal.

```bash
$  ../str2hex "Hola Guzman"
=> 486F6C612047757A6D616E
```

1. Criframos

```bash
$  ./rsa 39 BB $(../str2hex "Hola Guzman")
=> 8C6F939E200E53850A9E6E
```

### Operaciones subyacentes

Pero de dónde salen esos números:

Si nos fijamos en la `H` de `Hola Guzman` vemos que tiene por código ASCII el 72 (48h). Si $` d = 57 `$ y $` n = 187 `$, entonces:

```math
c = m^d \quad (mod \; n) \\
c = 72^{57} \quad ( mod \; 187)
```

que podemos hacer usando el comando `sqam`

```bash
$ ./sqam 72 57 187
=> 140
$ printf "%02X\n" 140
=> 8C
```

8C es el primer byte cifrado que vemos en la salida del comando `rsa`.

Si supieramos cuál es la otra clave que va con el 57 podríamos descifrar el código:

```bash
$ ./sqam 72 <e> 187
=> 72
$ printf "%02X\n" 72
=> 48
$ echo -e "\x48"
=> H
```



## Descifrado

### Conociendo la clave

1. Sabiendo el totiente (BBh) y la clave (49h)

```bash
$ ./rsa 49 BB 8C6F939E200E53850A9E6E
=> 486F6C612047757A6D616E

$ ./rsa 49 BB 8C6F939E200E53850A9E6E | ../hex2str
=> Hola Guzman
```

### Conociendo n

1. Para calcular $` e `$ 
1. tenemos que conocer el totiente $` \varphi(n) `$
1. y a su vez necesitamos los números primos $` p `$ y $` q `$

Pero, ¡espera! No te pongas a dividir como un loco, que hay un script que
se llama `ffa` e implementa el _algoritmo rápido de Fermat_ y vale para
encontrar los factores primos a toda pastilla.

Simplemente escribe:

```bash
$ ./ffa 187
=> 17 11
```

¡Hala! Ya sabes que el totiente es 160.

Y ahora tienes que encontrar un número $` e `$  que cumpla que

```math
57 \cdot e \equiv 1 \quad (mod \; 160 )
```

¡Eso sí que es una pesadez!

Salvo por un pequeño detalle. Alguien se ha dado cuenta de que la ecuación anterior
quiere decir que $` 57 \cdot e `$  tiene que valer 1, 161, 321, 481,...

Es decir, $` k `$  veces 160 mas 1.

```math
57 \cdot e = k \cdot 160 + 1 
```

O sea,

```math
57 \cdot e - k \cdot 160 = 1 
```

Pues hay un script que se llama `gcd` que averigua los valores de $` e `$  y $` k `$ .

Tan solo escribe:

```bash
$ ./gcd -b 57 160
=> 73 -26
```

La opción `b` es porque la ecuación anterior se llama identidad de Bezout y es lo que queremos
que nos resuelva el script.

73 es el valor de $` e `$. Del valor de $` k `$ sencillamente puedes pasar. No lo quieres para nada.

¡Enhorabuena! Ya has encontrado la clave.

Estos son los algoritmos que hay. No hay mucho más a no ser que tengas un ordenador cuántico o que
optimices este algoritmo para que tus tarjetas gráficas lo hagan a toda pastilla.

Lo que si se puede hacer es automatizar todo este proceso para que no te tengas que ocupar de los detalles. Pero de `gpg` hablamos en el siguiente capítulo.

Puedes encontrar scripts automatizados para encontrar claves wifi e instalarlos en tu GNU/linux pero también puedes ya empezar a echar un vistazo a algunas distribuciones que ya vienen con ellos instalados.

## Distribuciones

Un breve resumen de algunas distribuciones:

| Nombre | Descripción |
|--------|-------------|
| Kali Linux | Es la plataforma más avanzada de hacking y pen testing. |
| BackBox | Distribución minimalista (ligera) por si tienes que ejecutarla en un dispositivo poco potente. |
| ParrotOS | Distribución orientada a la nube. |
| BlackArch | Basada en Arch, tiene miles de herramientas de pen testing. |
| Samurai Web Testing Framework | Penetración en la web. |
| Pentoo Linux | Basada en Gentoo. |
| CAINE | Computer Aided Investigative Environment. Análisis Forense. |
| Network Security Toolkit | Basada en Fedora se especializa en la red. |
| Fedora Security Spin | Auditoría y testeo. Buena para aprender. |
| ArchStrike | Herramientas de penetración categorizadas por paquetes. |





