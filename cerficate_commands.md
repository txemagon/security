# Commands

## Creating a self signed certificate

From: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/

STEP 1


Generate a key pair by means of,

```bash
openssl genrsa -des3 -passout pass:x -out keypair.key 2048
ssh-keygen -b2048 -t rsa # Alternative way
```

Take into account the observations about des3 (symmetric and slow). DES3 is here used to protect the key.


STEP 2

Then extract the private into httpd foder.

```bash
sudo mkdir /etc/httpd/httpscertificate

openssl -rsa -passin pass:x -in keypair.key -out /etc/httpd/httpscertificate/012.345.878.90.key # Name it after the ip address of your server.

rm keypair.key
```

STEP 3

Create a "Certificate Signing Request" (CSR) File

This file can be signed by ourselves or sended to a CA

```bash
openssl req -new -key /etc/httpd/httpscertificate/012.245.678.90.key -out /etc/httpd/httpscertificate/012.345.678.90.csr
```

STEP 4

Creating the certificate ".crt" file

With the CSR and the KEY we can sign our own CRT.

```bash
openssl x509 -req -days 365 -in /etc/httpd/httpscertificate/012.345.678.90.csr -signkey /etc/httpd/httpscertificate/012.345.678.90.key -out /etc/httpd/httpscertificate/012.345.678.90.crt

```

STEP 5

Configure Apache to use the files

Ensure mod_ssl is installed in apache:

```bash
sudo a2enmod ssl
```

Edit `/etc/httpd/conf.d/ssl.conf` and change the lines

```
SSLCertificateFile
SSLCertificateKeyFile
```

to point to corresponding paths

and restart apache

```bash
sudo apachectl restart
```

## How to create the certificate.bundle file?

In the WWW trust chain usually looks like this:

- Trusted certificate from `/etc/ssl/certs/'
- unknown intermediate certificates, possibly cross signed by another CA
- your certificate (certificate.crt)

Evaluation takes place from bottom to top:

1. Your ceritificate is read.
1. Then the unknown intermediate certificate,
1. then cross-signing certificate
1. finally, /etc/ssl/certs

The ca-bundle is made in the exact same order, an intermediate certificate will sign your certificate.

Usually th CA will provide the CA-bundle file. If not you'll have to `cat` them.

The files need to be in `PEM` format. If it is in `DER` (binary) format. To convert it,

```bash
openssl x509 -in mycert.crt -out mycert.pem -outform PEM
```

or

```bash
openssl x509 -inform DER -in yourdownloaded.crt -out outcert.pem -text
```

Example: If you have the signing order


```
crossintermediate.crt >  crossigned.crt > intermediate1.crt > intermediate2.crt > certificate.crt
```

Do,
```bash
cat intermediate2.crt intermediate1.crt crossigned.crt crossintermediate.crt > certificate.bundle
```

## Verify certificates

You can generate a self signed Root certificate
and an intermediate certificate and validate the
intermediate with this command:


```bash
openssl verify -CAfile RootCert.pem -untrusted Intermediate.pem UserCert.pem
```

From https://stackoverflow.com/questions/25482199/verify-a-certificate-chain-using-openssl-verify

you can make apache verify the full trust chain with:

```bash

#!/bin/bash
# This Works is placed under the terms of the Copyright Less License,
# see file COPYRIGHT.CLL.  USE AT OWN RISK, ABSOLUTELY NO WARRANTY. 
#
# COPYRIGHT.CLL can be found at http://permalink.de/tino/cll
# (CLL is CC0 as long as not covered by any Copyright)

OOPS() { echo "OOPS: $*" >&2; exit 23; }

PID=
kick() { [ -n "$PID" ] && kill "$PID" && sleep .2; PID=; }
trap 'kick' 0

serve()
{
kick
PID=
openssl s_server -key "$KEY" -cert "$CRT" "$@" -www &
PID=$!
sleep .5    # give it time to startup
}

check()
{
while read -r line
do
    case "$line" in
    'Verify return code: 0 (ok)')   return 0;;
    'Verify return code: '*)    return 1;;
#   *)  echo "::: $line :::";;
    esac
done < <(echo | openssl s_client -verify 8 -CApath /etc/ssl/certs/)
OOPS "Something failed, verification output not found!"
return 2
}

ARG="${1%.}"
KEY="$ARG.key"
CRT="$ARG.crt"
BND="$ARG.bundle"

for a in "$KEY" "$CRT" "$BND"
do
    [ -s "$a" ] || OOPS "missing $a"
done

serve
check && echo "!!! =========> CA-Bundle is not needed! <========"
echo
serve -CAfile "$BND"
check
ret=$?
kick

echo
case $ret in
0)  echo "EVERYTHING OK"
    echo "SSLCertificateKeyFile $KEY"
    echo "SSLCertificateFile    $CRT"
    echo "SSLCACertificateFile  $BND"
    ;;
*)  echo "!!! =========> something is wrong, verification failed! <======== ($ret)";;
esac

exit $ret

```

Usage:

1. Install trusted CA root at `/etc/ssl/certs`
1. Store these 3 files:
    - `DIR/certificate.crt` which contains the certificate.
    - `DIR/certificate.key` which contains the secret key for your webservice (without passphrase)
    - `DIR/certificate.bundle` which contains the CA-Bundle. On how to prepare the bundle, see below.

1. Now run the script: `./check DIR/certificate` (this assumes that the script is named `check` in the current directory)

Notice:

1. There is a very unlikely case that the script outputs `CA-Bundle is not needed`. This means, that you (read: `/etc/ssl/certs/`) already trusts the signing certificate. But this is highly unlikely in the WWW.
1. For this test port 4433 must be unused on your workstation. And better only run this in a secure environment, as it opens port 4433 shortly to the public, which might see foreign connects in a hostile environment.
