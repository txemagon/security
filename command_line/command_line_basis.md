# Command Line Basis
From "Networking and Security with Linux": https://sandilands.info/nsl/


The command help for `openssl <verb>` can be found at `man <verb>`. Try for instance `man enc`.

## Random Numbers

A Linear Congruential Generator (LCG) from 0 to 32767. It's not a strong Pseudo
Random Number Generator (PRNG). Should not be used to generate keys.

```bash
$ echo $RANDOM
```

The true strong PRNG is urandom

```bash
$ cat /dev/urandom | xxd -l 8 -b

00000000: 10101101 11000111 01001100 01010110 01010011 00001100  ..LVS.
00000006: 10001111 11011000                                      ..
```

```bash
$ cat /dev/urandom | xxd -l18 -g 16
00000000: 0324e271345785586cc5402584e8dc35  .$.q4W.Xl.@%...5
```

```bash
$ cat /dev/urandom | xxd -l 16 -g 16 | cut -d " " -f 2
3aba5cf9771e4f6204839f1c411e7f0b
```

OpenSSL also has a strong PRNG

```bash
openssl rand -hex 8
d7a6cca8718a67c5
```


You can also dump it to a binary file:

```bash
$ openssl rand -out rand1.bin 8
$ xxd -b -g 8 -c 8 rand1.bin | cut -d" " -f2
1010001110001101101011000010110000111101101111011110111100000101
```

## Symmetric Key Encryption Basics


### Listing

To list algorithms:

```bash
$ openssl list cipher-algorithms

Legacy:
  AES-128-CBC
  AES-128-CBC-HMAC-SHA1
  AES-128-CBC-HMAC-SHA256
  id-aes128-CCM
.
.
  SM4-CFB
  SM4-CTR
  SM4-ECB
  SM4-OFB

Provided:
  { 2.16.840.1.101.3.4.1.47, aes-256-ccm, id-aes256-CCM } @ default
  { 2.16.840.1.101.3.4.1.3, AES-128-OFB } @ default
  { 2.16.840.1.101.3.4.1.1, AES-128-ECB } @ default
.
.
  DES-EDE-OFB @ default
  DES-EDE-CFB @ default
  ChaCha20 @ default
  ChaCha20-Poly1305 @ default

```

### DES

To encrypt with Data Encryption ALgorithm (DES) in Electronic Code Book (ECB) mode, type:

```bash
$ openssl enc -des-ecb -in plaintext1.in -out output/cyphertext1.bin -provider legacy -provider default
$ cat output/cyphertext1.bin | od -tcx1 | head
0000000   S   a   l   t   e   d   _   _ 372 362 303   \ 177 362   \ 316
         53  61  6c  74  65  64  5f  5f  fa  f2  c3  5c  7f  f2  5c  ce
0000020 351   L 267 342 237 211 270   b 260 360 245   |   } 360   R 227
         e9  4c  b7  e2  9f  89  b8  62  b0  f0  a5  7c  7d  f0  52  97
0000040 334   o 302   } 341   J   j 316   5 243 215 257 365 302 330 347
         dc  6f  c2  7d  e1  4a  6a  ce  35  a3  8d  af  f5  c2  d8  e7
0000060   Z   c   q   *   $ 337 337 272 211 370   ?   R 356  \n   f   Y
         5a  63  71  2a  24  df  df  ba  89  f8  3f  52  ee  0a  66  59
0000100 261   ] 321 207   M   X 354 377   j 270   P   % 177 335 340 017
         b1  5d  d1  87  4d  58  ec  ff  6a  b8  50  25  7f  dd  e0  0f
```

As long as DES is deprecated we have to use the legacy provider. More on DES and modes will be coming in a separate file.

The salt (to prevent rainbow tables) is `FA:F2:C3:5C 7F:F2:5C:CE`

To decrypt, use -d option:

```bash
openssl enc -d -des-ecb -in output/cyphertext1.bin -provider legacy -provider default
```

### AES

Aes in CTR Mode of Operation. In addtion to the key, an Initialialisation Vector/Value(IV) is needed.

```bash
$ openssl enc -aes-128-ctr -in plaintext2.in -out output/cyphertext2.bin -K 0123456789abcdef0123456789abcdef -iv 00000000000000000000000000000000

hex string is too short, padding with zero bytes to length

$ openssl enc -d -aes-128-ctr -in output/cyphertext2.bin -K 0123456789abcdef0123456789abcdef -iv 00000000000000000000000000000000
```

openssl command line is not POSIX compliant and spaces cannot be removed in between options and arguments.


## Hash and MAC Functions

Main Algorithms:

1. MD5
1. Secure Hash Algorithm (SHA)
1. Hash-based MAC (HMAC)

All supported:

```bash
$ openssl list -digest-algorithms
RSA-MD4 => MD4
RSA-MD5 => MD5
RSA-RIPEMD160 => RIPEMD160
RSA-SHA1 => SHA1
RSA-SHA1-2 => RSA-SHA1
RSA-SHA224 => SHA224
RSA-SHA256 => SHA256
RSA-SHA3-224 => SHA3-224
.
.
.
SHA512-256
sha512-256WithRSAEncryption => SHA512-256
sha512WithRSAEncryption => SHA512
SHAKE128
SHAKE256
SM3
sm3WithRSAEncryption => SM3
ssl3-md5 => MD5
ssl3-sha1 => SHA1
whirlpool

```

Some examples:

### MD5

```bash
$ openssl dgst -md5 <<< "Hello"
(stdin)= 09f7e02f1290be211da707a266f153b3

$ openssl dgst -md5 <<< "Hello."
(stdin)= 5083abdbc540c4a95ea195be6e3a9489

$ openssl dgst -md5 plaintext3.in
MD5(plaintext3.in)= 831335372c0c0fc53b99873290316607
```


### SHA256

```bash
$ openssl dgst -sha256 <<< "Hello world"
(stdin)= 1894a19c85ba153acbf743ac4e43fc004c891604b26f8c69e1e83ea2afc7c48f
```

Into a binary file:

```bash
$ openssl dgst -sha256 -binary -out output/dgst3.bin plaintext3.in 

$ xxd output/dgst3.bin
00000000: 67fb 3fb1 8299 dd2c 59e0 bd84 4076 ecb8  g.?....,Y...@v..
00000010: 50fd c8a0 a84d 4dff a3bd d188 7c61 50bf  P....MM.....|aP.

$ openssl dgst -sha256 plaintext3.in 
SHA256(plaintext3.in)= 67fb3fb18299dd2c59e0bd844076ecb850fdc8a0a84d4dffa3bdd1887c6150bf

```

In linux we have the commands_

- `md5sum`
- `sha1sum`, `sha224sum`,`sha256sum`, ...

## Symmetric Key Encryption Padding and Modes of Operation

If we create a text with a length multiple of 8:

```bash
$ echo -n "Hello. This is our super secret message. Keep it secret please. Goodbye." > plaintext.txt

$ wc -m plaintext.txt
$ xxd -c 8 plaintext.txt

```

To encrypt with `DES-ECB` we need two randomly chosen numbers for the secret key and the IV.

```bash
$ openssl rand -hex 8
a54f65043eb7901f

$ openssl rand -hex 8
3bc7a56647895389
```

And encryption

```bash
$ openssl enc -des-ecb -e -in plaintext.txt -out output/cyphertext.bin -K a54f65043eb7901f -nopad

xxd -c 8 output/cyphertext.bin 
00000000: 4133 14e3 ea09 b8a5  A3......
00000008: 6b46 2c10 e879 bff8  kF,..y..
00000010: fb9b adeb d8e6 4b93  ......K.
00000018: 4e1b 5e5c 3c45 d95c  N.^\<E.\ <- This lines are equal
00000020: 2a74 8e19 ffda 11c2  *t......
00000028: 3c1d 100b a541 892b  <....A.+
00000030: 4e1b 5e5c 3c45 d95c  N.^\<E.\ <- since they encode the same word (secure).
00000038: 7a47 05b9 7166 4a73  zG..qfJs
00000040: d90b 4716 732a edee  ..G.s*..
```

## RSA and Digital Signatures

Generate a RSA key pair with `genpkey`:

```bash
$ openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -pkeyopt rsa_keygen_pubexp:3 -out privkey-alice.pem
.+..........+..............+....+......+.........+............+..............+.........+...+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.......+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.....+............+..................+...+...............+........................+...+...+......+...............+...+...+.........+.........+............+...+......+..............................+......+.........+......+.........+...+.....................+....................................+......+.........+...+..................+............+...+..................+...............+...............+......+.........+......+...+......+...............+......+...+..................+............+...+...+......+...+......+........................+............+.........+......+...............+...+.....................+......+...+......+.........+............+...+...........................+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.+..+.........+...+...+.......+..+....+..+....+........+.+.....+....+..+.+..................+......+........+...+....+......+...+..+............+...+....+.....+....+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*....+........+.+............+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.......+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

$ cat privkey-alice.pem
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDviJhCtCfR3FCK
QXt1UX2I9I71uMbIylA1pIyecdDNN4MM8ElSeWa6SRgTP2F2MXe0fULECMiWw0xR
d97+25JcTUxaQthTZhJK8nu/FHknOP5R9PnCrayscV8sgB/k1jv07U8SYtmSFeuJ
aabXTe2JcaivCyc/HxNSCXfXl9fvZlyQ3Jn9z9rV1shYdh6ufbR+JUybScq7jpzT
VU6Ud163GFXXEiLrEUgOUTeKtyaOnlVUuzZeW4klNtiVU4X6GcTC0W7JsK23CjB4
G9jxvFTv0JFgrozHISqXNFy4iZszZ0vDftmJq6hBIzFYPIw9CzKoFtNq9PKxwZ54
wRKCxeNjAgEDAoIBACfsGWBzW/hPYsG1lJONlOwown5JdnbMYrObbMUS+CIz6yzS
tuMUO8m22ViKkD5dk/NqNctWzBkgjLg+pSp57boM4g8LJA3mWGHTFJ/Yvtve1Q2o
1Esc8hy9j9zABVDOX1N84oMQeZhY/Jbm8SPiUkGS8XKB29/agzhW6U6ZTqfmEi/B
mO+YIjKOWuTzmK4FeBDRhQT3ENZ5dbWFd7FLcFq9T7jnaCZ5K2oCdTnDwu+IkOwN
c8xQIB9Du5eXS+4QPC/tUN6MW24gBYC0nrrLBWJlvZr0MJzOWcYZUxMYAHWN36k0
OiMCezuNafQlWaHw9vv8hoddsD/v9cgC79vwLHcCgYEA/oXWwXp9feOJm+aRb7pu
JhDjEjxHNHrxd0QG31oXkAhDC2Mz8fI+6ucUYegLXt3qYG0yBFFOZswRcb6Tg5Zr
ZSgqb5PBau2eS5lT+DXlyiH63ub42G66UCatviud12RzUOZvlc7ShTNGazS5c9a7
XlGUH/fgH0vtsFFT19wFQf8CgYEA8Ox8QuXBj8L3CxQvGt/uvghZHEE4MTnEY04t
oRKbLI5lbB5xwBH7WKsuFkgVNhOAj184e0MsYZ2PAUk0Os9M9nsXGf6lHjSrw9qM
KFsUan9zQNf8kKuQv8vurBpbwQ2gvOXV5ujKP6iMSmqi/pqqCm6XjEbiseY0K3db
m38flp0CgYEAqa6PK6b+U+0GZ+8Ln9GexAtCDCgveFH2T4KvP5FlCrAsskIioUwp
8e9i6/AHlJPxlZ4hWDY0RIgLoSm3rQ7yQ3AcSmKA8fO+3RDipXlD3BanP0Sl5Z8m
4BnJKXJpOkL3i0RKY983A3eER3h7oo8nlDZiv/qVajKedYuNOpKuK/8CgYEAoJ2o
LJkrtSyksg10vJVJ1AWQvYDQINEtl4lzwLcSHbRDnWmhKrankHIeuYVjeWJVtOol
p4IdlmkKANt4JzTd+adku/8Yvs3H1+cIGudi8apM1eVTCx0LKof0crw9K15rKJk5
RJsxf8Wy3EcXVGccBvRlCC9By+7Nck+SZ6oVDxMCgYEA6XxCEOfVfmxrVSXQx2Hq
BLC+7crpUERFUCmWzwL6lkmR8FcFyabDw0cispHFIr5hDNKiyjZwNRNr6BLH7fo9
6CJHvLcCn1f7H4HQL3NOnFb1JMtaoAsnWoOkw7A4Dl1wE1PL+RHXuVHsX5rKzCx3
RDlHDp+u08bXGjwd0phXO8g=
-----END PRIVATE KEY-----

```

The key is base64 coded. To see it:

```bash
$ openssl pkey  -in privkey-alice.pem -text
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDviJhCtCfR3FCK
QXt1UX2I9I71uMbIylA1pIyecdDNN4MM8ElSeWa6SRgTP2F2MXe0fULECMiWw0xR
d97+25JcTUxaQthTZhJK8nu/FHknOP5R9PnCrayscV8sgB/k1jv07U8SYtmSFeuJ
aabXTe2JcaivCyc/HxNSCXfXl9fvZlyQ3Jn9z9rV1shYdh6ufbR+JUybScq7jpzT
VU6Ud163GFXXEiLrEUgOUTeKtyaOnlVUuzZeW4klNtiVU4X6GcTC0W7JsK23CjB4
G9jxvFTv0JFgrozHISqXNFy4iZszZ0vDftmJq6hBIzFYPIw9CzKoFtNq9PKxwZ54
wRKCxeNjAgEDAoIBACfsGWBzW/hPYsG1lJONlOwown5JdnbMYrObbMUS+CIz6yzS
tuMUO8m22ViKkD5dk/NqNctWzBkgjLg+pSp57boM4g8LJA3mWGHTFJ/Yvtve1Q2o
1Esc8hy9j9zABVDOX1N84oMQeZhY/Jbm8SPiUkGS8XKB29/agzhW6U6ZTqfmEi/B
mO+YIjKOWuTzmK4FeBDRhQT3ENZ5dbWFd7FLcFq9T7jnaCZ5K2oCdTnDwu+IkOwN
c8xQIB9Du5eXS+4QPC/tUN6MW24gBYC0nrrLBWJlvZr0MJzOWcYZUxMYAHWN36k0
OiMCezuNafQlWaHw9vv8hoddsD/v9cgC79vwLHcCgYEA/oXWwXp9feOJm+aRb7pu
JhDjEjxHNHrxd0QG31oXkAhDC2Mz8fI+6ucUYegLXt3qYG0yBFFOZswRcb6Tg5Zr
ZSgqb5PBau2eS5lT+DXlyiH63ub42G66UCatviud12RzUOZvlc7ShTNGazS5c9a7
XlGUH/fgH0vtsFFT19wFQf8CgYEA8Ox8QuXBj8L3CxQvGt/uvghZHEE4MTnEY04t
oRKbLI5lbB5xwBH7WKsuFkgVNhOAj184e0MsYZ2PAUk0Os9M9nsXGf6lHjSrw9qM
KFsUan9zQNf8kKuQv8vurBpbwQ2gvOXV5ujKP6iMSmqi/pqqCm6XjEbiseY0K3db
m38flp0CgYEAqa6PK6b+U+0GZ+8Ln9GexAtCDCgveFH2T4KvP5FlCrAsskIioUwp
8e9i6/AHlJPxlZ4hWDY0RIgLoSm3rQ7yQ3AcSmKA8fO+3RDipXlD3BanP0Sl5Z8m
4BnJKXJpOkL3i0RKY983A3eER3h7oo8nlDZiv/qVajKedYuNOpKuK/8CgYEAoJ2o
LJkrtSyksg10vJVJ1AWQvYDQINEtl4lzwLcSHbRDnWmhKrankHIeuYVjeWJVtOol
p4IdlmkKANt4JzTd+adku/8Yvs3H1+cIGudi8apM1eVTCx0LKof0crw9K15rKJk5
RJsxf8Wy3EcXVGccBvRlCC9By+7Nck+SZ6oVDxMCgYEA6XxCEOfVfmxrVSXQx2Hq
BLC+7crpUERFUCmWzwL6lkmR8FcFyabDw0cispHFIr5hDNKiyjZwNRNr6BLH7fo9
6CJHvLcCn1f7H4HQL3NOnFb1JMtaoAsnWoOkw7A4Dl1wE1PL+RHXuVHsX5rKzCx3
RDlHDp+u08bXGjwd0phXO8g=
-----END PRIVATE KEY-----
Private-Key: (2048 bit, 2 primes)
modulus:
    00:ef:88:98:42:b4:27:d1:dc:50:8a:41:7b:75:51:
    7d:88:f4:8e:f5:b8:c6:c8:ca:50:35:a4:8c:9e:71:
    d0:cd:37:83:0c:f0:49:52:79:66:ba:49:18:13:3f:
    61:76:31:77:b4:7d:42:c4:08:c8:96:c3:4c:51:77:
    de:fe:db:92:5c:4d:4c:5a:42:d8:53:66:12:4a:f2:
    7b:bf:14:79:27:38:fe:51:f4:f9:c2:ad:ac:ac:71:
    5f:2c:80:1f:e4:d6:3b:f4:ed:4f:12:62:d9:92:15:
    eb:89:69:a6:d7:4d:ed:89:71:a8:af:0b:27:3f:1f:
    13:52:09:77:d7:97:d7:ef:66:5c:90:dc:99:fd:cf:
    da:d5:d6:c8:58:76:1e:ae:7d:b4:7e:25:4c:9b:49:
    ca:bb:8e:9c:d3:55:4e:94:77:5e:b7:18:55:d7:12:
    22:eb:11:48:0e:51:37:8a:b7:26:8e:9e:55:54:bb:
    36:5e:5b:89:25:36:d8:95:53:85:fa:19:c4:c2:d1:
    6e:c9:b0:ad:b7:0a:30:78:1b:d8:f1:bc:54:ef:d0:
    91:60:ae:8c:c7:21:2a:97:34:5c:b8:89:9b:33:67:
    4b:c3:7e:d9:89:ab:a8:41:23:31:58:3c:8c:3d:0b:
    32:a8:16:d3:6a:f4:f2:b1:c1:9e:78:c1:12:82:c5:
    e3:63
publicExponent: 3 (0x3)
privateExponent:
    27:ec:19:60:73:5b:f8:4f:62:c1:b5:94:93:8d:94:
    ec:28:c2:7e:49:76:76:cc:62:b3:9b:6c:c5:12:f8:
    22:33:eb:2c:d2:b6:e3:14:3b:c9:b6:d9:58:8a:90:
    3e:5d:93:f3:6a:35:cb:56:cc:19:20:8c:b8:3e:a5:
    2a:79:ed:ba:0c:e2:0f:0b:24:0d:e6:58:61:d3:14:
    9f:d8:be:db:de:d5:0d:a8:d4:4b:1c:f2:1c:bd:8f:
    dc:c0:05:50:ce:5f:53:7c:e2:83:10:79:98:58:fc:
    96:e6:f1:23:e2:52:41:92:f1:72:81:db:df:da:83:
    38:56:e9:4e:99:4e:a7:e6:12:2f:c1:98:ef:98:22:
    32:8e:5a:e4:f3:98:ae:05:78:10:d1:85:04:f7:10:
    d6:79:75:b5:85:77:b1:4b:70:5a:bd:4f:b8:e7:68:
    26:79:2b:6a:02:75:39:c3:c2:ef:88:90:ec:0d:73:
    cc:50:20:1f:43:bb:97:97:4b:ee:10:3c:2f:ed:50:
    de:8c:5b:6e:20:05:80:b4:9e:ba:cb:05:62:65:bd:
    9a:f4:30:9c:ce:59:c6:19:53:13:18:00:75:8d:df:
    a9:34:3a:23:02:7b:3b:8d:69:f4:25:59:a1:f0:f6:
    fb:fc:86:87:5d:b0:3f:ef:f5:c8:02:ef:db:f0:2c:
    77
prime1:
    00:fe:85:d6:c1:7a:7d:7d:e3:89:9b:e6:91:6f:ba:
    6e:26:10:e3:12:3c:47:34:7a:f1:77:44:06:df:5a:
    17:90:08:43:0b:63:33:f1:f2:3e:ea:e7:14:61:e8:
    0b:5e:dd:ea:60:6d:32:04:51:4e:66:cc:11:71:be:
    93:83:96:6b:65:28:2a:6f:93:c1:6a:ed:9e:4b:99:
    53:f8:35:e5:ca:21:fa:de:e6:f8:d8:6e:ba:50:26:
    ad:be:2b:9d:d7:64:73:50:e6:6f:95:ce:d2:85:33:
    46:6b:34:b9:73:d6:bb:5e:51:94:1f:f7:e0:1f:4b:
    ed:b0:51:53:d7:dc:05:41:ff
prime2:
    00:f0:ec:7c:42:e5:c1:8f:c2:f7:0b:14:2f:1a:df:
    ee:be:08:59:1c:41:38:31:39:c4:63:4e:2d:a1:12:
    9b:2c:8e:65:6c:1e:71:c0:11:fb:58:ab:2e:16:48:
    15:36:13:80:8f:5f:38:7b:43:2c:61:9d:8f:01:49:
    34:3a:cf:4c:f6:7b:17:19:fe:a5:1e:34:ab:c3:da:
    8c:28:5b:14:6a:7f:73:40:d7:fc:90:ab:90:bf:cb:
    ee:ac:1a:5b:c1:0d:a0:bc:e5:d5:e6:e8:ca:3f:a8:
    8c:4a:6a:a2:fe:9a:aa:0a:6e:97:8c:46:e2:b1:e6:
    34:2b:77:5b:9b:7f:1f:96:9d
exponent1:
    00:a9:ae:8f:2b:a6:fe:53:ed:06:67:ef:0b:9f:d1:
    9e:c4:0b:42:0c:28:2f:78:51:f6:4f:82:af:3f:91:
    65:0a:b0:2c:b2:42:22:a1:4c:29:f1:ef:62:eb:f0:
    07:94:93:f1:95:9e:21:58:36:34:44:88:0b:a1:29:
    b7:ad:0e:f2:43:70:1c:4a:62:80:f1:f3:be:dd:10:
    e2:a5:79:43:dc:16:a7:3f:44:a5:e5:9f:26:e0:19:
    c9:29:72:69:3a:42:f7:8b:44:4a:63:df:37:03:77:
    84:47:78:7b:a2:8f:27:94:36:62:bf:fa:95:6a:32:
    9e:75:8b:8d:3a:92:ae:2b:ff
exponent2:
    00:a0:9d:a8:2c:99:2b:b5:2c:a4:b2:0d:74:bc:95:
    49:d4:05:90:bd:80:d0:20:d1:2d:97:89:73:c0:b7:
    12:1d:b4:43:9d:69:a1:2a:b6:a7:90:72:1e:b9:85:
    63:79:62:55:b4:ea:25:a7:82:1d:96:69:0a:00:db:
    78:27:34:dd:f9:a7:64:bb:ff:18:be:cd:c7:d7:e7:
    08:1a:e7:62:f1:aa:4c:d5:e5:53:0b:1d:0b:2a:87:
    f4:72:bc:3d:2b:5e:6b:28:99:39:44:9b:31:7f:c5:
    b2:dc:47:17:54:67:1c:06:f4:65:08:2f:41:cb:ee:
    cd:72:4f:92:67:aa:15:0f:13
coefficient:
    00:e9:7c:42:10:e7:d5:7e:6c:6b:55:25:d0:c7:61:
    ea:04:b0:be:ed:ca:e9:50:44:45:50:29:96:cf:02:
    fa:96:49:91:f0:57:05:c9:a6:c3:c3:47:22:b2:91:
    c5:22:be:61:0c:d2:a2:ca:36:70:35:13:6b:e8:12:
    c7:ed:fa:3d:e8:22:47:bc:b7:02:9f:57:fb:1f:81:
    d0:2f:73:4e:9c:56:f5:24:cb:5a:a0:0b:27:5a:83:
    a4:c3:b0:38:0e:5d:70:13:53:cb:f9:11:d7:b9:51:
    ec:5f:9a:ca:cc:2c:77:44:39:47:0e:9f:ae:d3:c6:
    d7:1a:3c:1d:d2:98:57:3b:c8

```

We can also see the key at offset

```bash
$ sed '1d;$d' privkey-alice.pem  | base64 -d | xxd 
00000000: 3082 04bd 0201 0030 0d06 092a 8648 86f7  0......0...*.H..
00000010: 0d01 0101 0500 0482 04a7 3082 04a3 0201  ..........0.....
00000020: 0002 8201 0100 ef88 9842 b427 d1dc 508a  .........B.'..P.
00000030: 417b 7551 7d88 f48e f5b8 c6c8 ca50 35a4  A{uQ}........P5.
00000040: 8c9e 71d0 cd37 830c f049 5279 66ba 4918  ..q..7...IRyf.I.
00000050: 133f 6176 3177 b47d 42c4 08c8 96c3 4c51  .?av1w.}B.....LQ
00000060: 77de fedb 925c 4d4c 5a42 d853 6612 4af2  w....\MLZB.Sf.J.
00000070: 7bbf 1479 2738 fe51 f4f9 c2ad acac 715f  {..y'8.Q......q_
00000080: 2c80 1fe4 d63b f4ed 4f12 62d9 9215 eb89  ,....;..O.b.....
00000090: 69a6 d74d ed89 71a8 af0b 273f 1f13 5209  i..M..q...'?..R.
000000a0: 77d7 97d7 ef66 5c90 dc99 fdcf dad5 d6c8  w....f\.........
000000b0: 5876 1eae 7db4 7e25 4c9b 49ca bb8e 9cd3  Xv..}.~%L.I.....
000000c0: 554e 9477 5eb7 1855 d712 22eb 1148 0e51  UN.w^..U.."..H.Q
000000d0: 378a b726 8e9e 5554 bb36 5e5b 8925 36d8  7..&..UT.6^[.%6.
000000e0: 9553 85fa 19c4 c2d1 6ec9 b0ad b70a 3078  .S......n.....0x
000000f0: 1bd8 f1bc 54ef d091 60ae 8cc7 212a 9734  ....T...`...!*.4
00000100: 5cb8 899b 3367 4bc3 7ed9 89ab a841 2331  \...3gK.~....A#1
00000110: 583c 8c3d 0b32 a816 d36a f4f2 b1c1 9e78  X<.=.2...j.....x
00000120: c112 82c5 e363 0201 0302 8201 0027 ec19  .....c.......'..
00000130: 6073 5bf8 4f62 c1b5 9493 8d94 ec28 c27e  `s[.Ob.......(.~
00000140: 4976 76cc 62b3 9b6c c512 f822 33eb 2cd2  Ivv.b..l..."3.,.
00000150: b6e3 143b c9b6 d958 8a90 3e5d 93f3 6a35  ...;...X..>]..j5
00000160: cb56 cc19 208c b83e a52a 79ed ba0c e20f  .V.. ..>.*y.....
00000170: 0b24 0de6 5861 d314 9fd8 bedb ded5 0da8  .$..Xa..........
00000180: d44b 1cf2 1cbd 8fdc c005 50ce 5f53 7ce2  .K........P._S|.
00000190: 8310 7998 58fc 96e6 f123 e252 4192 f172  ..y.X....#.RA..r
000001a0: 81db dfda 8338 56e9 4e99 4ea7 e612 2fc1  .....8V.N.N.../.
000001b0: 98ef 9822 328e 5ae4 f398 ae05 7810 d185  ..."2.Z.....x...
000001c0: 04f7 10d6 7975 b585 77b1 4b70 5abd 4fb8  ....yu..w.KpZ.O.
000001d0: e768 2679 2b6a 0275 39c3 c2ef 8890 ec0d  .h&y+j.u9.......
000001e0: 73cc 5020 1f43 bb97 974b ee10 3c2f ed50  s.P .C...K..</.P
000001f0: de8c 5b6e 2005 80b4 9eba cb05 6265 bd9a  ..[n .......be..
00000200: f430 9cce 59c6 1953 1318 0075 8ddf a934  .0..Y..S...u...4
00000210: 3a23 027b 3b8d 69f4 2559 a1f0 f6fb fc86  :#.{;.i.%Y......
00000220: 875d b03f eff5 c802 efdb f02c 7702 8181  .].?.......,w...
00000230: 00fe 85d6 c17a 7d7d e389 9be6 916f ba6e  .....z}}.....o.n
00000240: 2610 e312 3c47 347a f177 4406 df5a 1790  &...<G4z.wD..Z..
00000250: 0843 0b63 33f1 f23e eae7 1461 e80b 5edd  .C.c3..>...a..^.
00000260: ea60 6d32 0451 4e66 cc11 71be 9383 966b  .`m2.QNf..q....k
00000270: 6528 2a6f 93c1 6aed 9e4b 9953 f835 e5ca  e(*o..j..K.S.5..
00000280: 21fa dee6 f8d8 6eba 5026 adbe 2b9d d764  !.....n.P&..+..d
00000290: 7350 e66f 95ce d285 3346 6b34 b973 d6bb  sP.o....3Fk4.s..
000002a0: 5e51 941f f7e0 1f4b edb0 5153 d7dc 0541  ^Q.....K..QS...A
000002b0: ff02 8181 00f0 ec7c 42e5 c18f c2f7 0b14  .......|B.......
000002c0: 2f1a dfee be08 591c 4138 3139 c463 4e2d  /.....Y.A819.cN-
000002d0: a112 9b2c 8e65 6c1e 71c0 11fb 58ab 2e16  ...,.el.q...X...
000002e0: 4815 3613 808f 5f38 7b43 2c61 9d8f 0149  H.6..._8{C,a...I
000002f0: 343a cf4c f67b 1719 fea5 1e34 abc3 da8c  4:.L.{.....4....
00000300: 285b 146a 7f73 40d7 fc90 ab90 bfcb eeac  ([.j.s@.........
00000310: 1a5b c10d a0bc e5d5 e6e8 ca3f a88c 4a6a  .[.........?..Jj
00000320: a2fe 9aaa 0a6e 978c 46e2 b1e6 342b 775b  .....n..F...4+w[
00000330: 9b7f 1f96 9d02 8181 00a9 ae8f 2ba6 fe53  ............+..S
00000340: ed06 67ef 0b9f d19e c40b 420c 282f 7851  ..g.......B.(/xQ
00000350: f64f 82af 3f91 650a b02c b242 22a1 4c29  .O..?.e..,.B".L)
00000360: f1ef 62eb f007 9493 f195 9e21 5836 3444  ..b........!X64D
00000370: 880b a129 b7ad 0ef2 4370 1c4a 6280 f1f3  ...)....Cp.Jb...
00000380: bedd 10e2 a579 43dc 16a7 3f44 a5e5 9f26  .....yC...?D...&
00000390: e019 c929 7269 3a42 f78b 444a 63df 3703  ...)ri:B..DJc.7.
000003a0: 7784 4778 7ba2 8f27 9436 62bf fa95 6a32  w.Gx{..'.6b...j2
000003b0: 9e75 8b8d 3a92 ae2b ff02 8181 00a0 9da8  .u..:..+........
000003c0: 2c99 2bb5 2ca4 b20d 74bc 9549 d405 90bd  ,.+.,...t..I....
000003d0: 80d0 20d1 2d97 8973 c0b7 121d b443 9d69  .. .-..s.....C.i
000003e0: a12a b6a7 9072 1eb9 8563 7962 55b4 ea25  .*...r...cybU..%
000003f0: a782 1d96 690a 00db 7827 34dd f9a7 64bb  ....i...x'4...d.
00000400: ff18 becd c7d7 e708 1ae7 62f1 aa4c d5e5  ..........b..L..
00000410: 530b 1d0b 2a87 f472 bc3d 2b5e 6b28 9939  S...*..r.=+^k(.9
00000420: 449b 317f c5b2 dc47 1754 671c 06f4 6508  D.1....G.Tg...e.
00000430: 2f41 cbee cd72 4f92 67aa 150f 1302 8181  /A...rO.g.......
00000440: 00e9 7c42 10e7 d57e 6c6b 5525 d0c7 61ea  ..|B...~lkU%..a.
00000450: 04b0 beed cae9 5044 4550 2996 cf02 fa96  ......PDEP).....
00000460: 4991 f057 05c9 a6c3 c347 22b2 91c5 22be  I..W.....G"...".
00000470: 610c d2a2 ca36 7035 136b e812 c7ed fa3d  a....6p5.k.....=
00000480: e822 47bc b702 9f57 fb1f 81d0 2f73 4e9c  ."G....W..../sN.
00000490: 56f5 24cb 5aa0 0b27 5a83 a4c3 b038 0e5d  V.$.Z..'Z....8.]
000004a0: 7013 53cb f911 d7b9 51ec 5f9a cacc 2c77  p.S.....Q._...,w
000004b0: 4439 470e 9fae d3c6 d71a 3c1d d298 573b  D9G.......<...W;
```

To output the public key to a file:

```bash
$ openssl pkey -in privkey-alice.pem -out pubkey-alice.pem -pubout
$ cat pubkey-alice.pem 
-----BEGIN PUBLIC KEY-----
MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEA74iYQrQn0dxQikF7dVF9
iPSO9bjGyMpQNaSMnnHQzTeDDPBJUnlmukkYEz9hdjF3tH1CxAjIlsNMUXfe/tuS
XE1MWkLYU2YSSvJ7vxR5Jzj+UfT5wq2srHFfLIAf5NY79O1PEmLZkhXriWmm103t
iXGorwsnPx8TUgl315fX72ZckNyZ/c/a1dbIWHYern20fiVMm0nKu46c01VOlHde
txhV1xIi6xFIDlE3ircmjp5VVLs2XluJJTbYlVOF+hnEwtFuybCttwoweBvY8bxU
79CRYK6MxyEqlzRcuImbM2dLw37ZiauoQSMxWDyMPQsyqBbTavTyscGeeMESgsXj
YwIBAw==
-----END PUBLIC KEY-----

$ openssl pkey -in pubkey-alice.pem -pubin -text
-----BEGIN PUBLIC KEY-----
MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEA74iYQrQn0dxQikF7dVF9
iPSO9bjGyMpQNaSMnnHQzTeDDPBJUnlmukkYEz9hdjF3tH1CxAjIlsNMUXfe/tuS
XE1MWkLYU2YSSvJ7vxR5Jzj+UfT5wq2srHFfLIAf5NY79O1PEmLZkhXriWmm103t
iXGorwsnPx8TUgl315fX72ZckNyZ/c/a1dbIWHYern20fiVMm0nKu46c01VOlHde
txhV1xIi6xFIDlE3ircmjp5VVLs2XluJJTbYlVOF+hnEwtFuybCttwoweBvY8bxU
79CRYK6MxyEqlzRcuImbM2dLw37ZiauoQSMxWDyMPQsyqBbTavTyscGeeMESgsXj
YwIBAw==
-----END PUBLIC KEY-----
Public-Key: (2048 bit)
Modulus:
    00:ef:88:98:42:b4:27:d1:dc:50:8a:41:7b:75:51:
    7d:88:f4:8e:f5:b8:c6:c8:ca:50:35:a4:8c:9e:71:
    d0:cd:37:83:0c:f0:49:52:79:66:ba:49:18:13:3f:
    61:76:31:77:b4:7d:42:c4:08:c8:96:c3:4c:51:77:
    de:fe:db:92:5c:4d:4c:5a:42:d8:53:66:12:4a:f2:
    7b:bf:14:79:27:38:fe:51:f4:f9:c2:ad:ac:ac:71:
    5f:2c:80:1f:e4:d6:3b:f4:ed:4f:12:62:d9:92:15:
    eb:89:69:a6:d7:4d:ed:89:71:a8:af:0b:27:3f:1f:
    13:52:09:77:d7:97:d7:ef:66:5c:90:dc:99:fd:cf:
    da:d5:d6:c8:58:76:1e:ae:7d:b4:7e:25:4c:9b:49:
    ca:bb:8e:9c:d3:55:4e:94:77:5e:b7:18:55:d7:12:
    22:eb:11:48:0e:51:37:8a:b7:26:8e:9e:55:54:bb:
    36:5e:5b:89:25:36:d8:95:53:85:fa:19:c4:c2:d1:
    6e:c9:b0:ad:b7:0a:30:78:1b:d8:f1:bc:54:ef:d0:
    91:60:ae:8c:c7:21:2a:97:34:5c:b8:89:9b:33:67:
    4b:c3:7e:d9:89:ab:a8:41:23:31:58:3c:8c:3d:0b:
    32:a8:16:d3:6a:f4:f2:b1:c1:9e:78:c1:12:82:c5:
    e3:63
Exponent: 3 (0x3)
```

### Example

```bash
$ echo "Alice sends this message to Bob." > message-alice.txt
$ openssl dgst -sha1 message-alice.txt
SHA1(message-alice.txt)= 9457109ce6c73bf92f637839605705087899330d
```

We can calculate the hash and sign it using the private key:

```bash
$ openssl dgst -sha1 -sign privkey-alice.pem -out sign-alice.bin message-alice.txt
```

With Bob public key the message will be signed

```bash
openssl pkeyutl -encrypt -in message-alice.txt -pubin -inkey pubkey-bob.pem -out ciphertext-alice.bin
```

Alice and Bob will exchange public keys and Alice will send cyphered text and signature.

Then Bob

Decryption
```bash
$ openssl pkeyutl -decrypt -in ciphertext-alice.bin -inkey privkey-bob.pem -out received-alice.txt
$ cat received-alice.txt
```


Verification
```bash
$ openssl dgst -sha1 -verify pubkey-alice.pem -signature sign-alice.bin received-alice.txt 
Verified OK
```


## Diffie-Hellman Secret Key Exchange (DHKE)

Or how to obtain a shared secret key.

```bash
$ openssl genpkey -genparam -algorithm DH -out dhparam.pem
.................................................................................................................................................................................................................................+............................+............+............................+.................................+........................................................................................................................................................................................................................................................................................................................................................+.................+..............................................................+................+..........+..........................................+....................................................................................................................................................+.............................................................................................................................+......................................................................................+.........................................+.........................................................................................................................................................................................................................................................................................................+..................................................................................................................................................................................................+...............+......................................................................................................................................................................................................................................................................................................................+........................................+...........+..............................................................................................................................................................................................................................................................................................................................................+.........................................................................................+.................................................................................+.............................................+.................................................................................+.........+.........+.........................................................................................................................................................................................................................+......................................................................................................+.........+..........................................................................................................................................................................................................................................................+...........................................................+...................................................................................................................+.......................................................................................+...........................................+....+...........................................................................................................................+...........................+.........................................................................................................................................+..........+.....................................................................................+...................................................................................................................................................................................................................................+...........................................................................................................................................................................................................+................................................................................................................................................................................................................................+.....................................................................+.................................................................................................................................................................................................+.............................................................+........................+.......................................................................+.......................+...........................+..............................................................................................................................................................................................................+........................................................................................................................................................................................................................................................................................................................................................................................................................................................................+............................................+.................................................................................+.............................................+............................+...................................+...............................................................+.......................+....................................+..................................+.........................+....................................................................+..+..............................................................................................................................................................................................................+...........+.......+................+.......................................+..........................+..........................................................................................................................................................+............+......................................................................................................................................................+.......................................................................................................+.......................................................................................+................................................................................................................................................................................................................................+..........................................................................+........................++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++
```



The generated file:

```bash
$ cat dhparam.pem
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEA6EtM2RQZVw6X1Vi/0Gh2wXyF6b0014OLR/sKdUKCkltg2KgBfha+
DbuvleGmKytiBaO4R8ydh1WZ4PudqNmUs80LCSIH11wVodfEvETPgwLLr875IXe5
HytYBpbCDTMHE7sYwU7chlNias6QUpxBWBroEdpfOWe6p9fLIG+SYcMCtHPFRmP9
EUe6DuGeNb3i4ZGXCHHa453tdiDaFkuUI+cLUApNQ9bzrYKoOwdTgv9c8mpD3JtL
AOlZN57Sa/IXugp5eI4S2DGriBsvor2XLqIr/1DrbRFjQ19ruIv0zdW5/nKZxCD8
QXa3FGfUJ5QVGGZRBqNz9QL5Po/abxWXZwIBAg==
-----END DH PARAMETERS-----

```

And now the uncoded (not cyphered) prime and generator:

```bash
$ openssl pkeyparam -in dhparam.pem -text
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEA6EtM2RQZVw6X1Vi/0Gh2wXyF6b0014OLR/sKdUKCkltg2KgBfha+
DbuvleGmKytiBaO4R8ydh1WZ4PudqNmUs80LCSIH11wVodfEvETPgwLLr875IXe5
HytYBpbCDTMHE7sYwU7chlNias6QUpxBWBroEdpfOWe6p9fLIG+SYcMCtHPFRmP9
EUe6DuGeNb3i4ZGXCHHa453tdiDaFkuUI+cLUApNQ9bzrYKoOwdTgv9c8mpD3JtL
AOlZN57Sa/IXugp5eI4S2DGriBsvor2XLqIr/1DrbRFjQ19ruIv0zdW5/nKZxCD8
QXa3FGfUJ5QVGGZRBqNz9QL5Po/abxWXZwIBAg==
-----END DH PARAMETERS-----
DH Parameters: (2048 bit)
P:   
    00:e8:4b:4c:d9:14:19:57:0e:97:d5:58:bf:d0:68:
    76:c1:7c:85:e9:bd:34:d7:83:8b:47:fb:0a:75:42:
    82:92:5b:60:d8:a8:01:7e:16:be:0d:bb:af:95:e1:
    a6:2b:2b:62:05:a3:b8:47:cc:9d:87:55:99:e0:fb:
    9d:a8:d9:94:b3:cd:0b:09:22:07:d7:5c:15:a1:d7:
    c4:bc:44:cf:83:02:cb:af:ce:f9:21:77:b9:1f:2b:
    58:06:96:c2:0d:33:07:13:bb:18:c1:4e:dc:86:53:
    62:6a:ce:90:52:9c:41:58:1a:e8:11:da:5f:39:67:
    ba:a7:d7:cb:20:6f:92:61:c3:02:b4:73:c5:46:63:
    fd:11:47:ba:0e:e1:9e:35:bd:e2:e1:91:97:08:71:
    da:e3:9d:ed:76:20:da:16:4b:94:23:e7:0b:50:0a:
    4d:43:d6:f3:ad:82:a8:3b:07:53:82:ff:5c:f2:6a:
    43:dc:9b:4b:00:e9:59:37:9e:d2:6b:f2:17:ba:0a:
    79:78:8e:12:d8:31:ab:88:1b:2f:a2:bd:97:2e:a2:
    2b:ff:50:eb:6d:11:63:43:5f:6b:b8:8b:f4:cd:d5:
    b9:fe:72:99:c4:20:fc:41:76:b7:14:67:d4:27:94:
    15:18:66:51:06:a3:73:f5:02:f9:3e:8f:da:6f:15:
    97:67
G:    2 (0x2)

```

With this parameters private keys can be used to generate a secret
shared key if dhparam.pem is shared to both users:

```bash
alice@node1: ~$ openssl genpkey -paramfile dhparam.pem -out dhprivkey-alice.pem

alice@node1: ~$ openssl pkey -in dhprivkey-alice.pem -text -noout
DH Private-Key: (2048 bit)
private-key:
    61:8a:b9:08:a5:fb:bc:32:f9:d1:1d:b9:fb:36:74:
    f5:49:4f:12:bf:da:13:51:dc:72:34:f9:f6:cc:a1:
    81:17:93:ac:32:79:d3:e4:e2:eb:a0:3b:5f:ef:7f:
    f7:ea:00:a9:e3:b7:b4:cd:81:e1:f6:67:4c:bf:bc:
    7c:6d:23:f2:8e:67:ee:3f:67:53:5b:34:4b:82:71:
    a1:d0:d9:d5:d0:4a:23:60:65:f1:7d:86:78:ff:84:
    a9:dc:82:7d:73:3c:a9:b0:95:8f:63:7a:37:d5:9d:
    38:7c:cb:6a:64:66:7d:f5:cc:2e:70:06:29:c3:fe:
    f2:c5:cc:b3:6f:75:a5:e2:52:28:1e:08:bf:58:4f:
    78:5c:1a:48:19:e7:70:ef:58:6d:98:00:be:33:2b:
    17:5b:3f:76:36:43:64:78:01:a8:9b:ae:17:c9:17:
    0e:81:1b:27:c3:9d:a2:e6:ed:7e:e7:4b:25:dd:3d:
    6d:af:a9:17:2a:17:f9:23:9f:1a:a7:37:5e:cd:53:
    db:1e:cf:5c:56:32:c6:d0:ae:a0:f1:c4:d5:17:e6:
    f4:b3:35:2b:54:22:10:05:94:14:a5:5c:3b:c8:68:
    5b:be:06:3f:a0:ef:ee:72:fe:ad:f4:e7:ea:60:c1:
    e1:f8:a4:82:0f:48:7b:bb:c2:01:18:ae:dd:5d:df:
    14
public-key:
    2e:27:2b:da:f1:98:17:ca:ed:c9:dd:6d:f7:56:7f:
    0f:6e:5b:2d:98:81:ef:cc:c6:bd:b0:b4:d4:e2:10:
    9c:2b:c1:34:be:ea:a3:a6:7c:7f:63:c9:11:66:5c:
    7e:21:b0:1e:a6:0b:e6:bf:e0:c4:e0:cf:dc:da:70:
    00:8b:4f:60:17:1e:b2:69:21:08:7d:e6:82:15:76:
    f5:96:ff:db:a9:12:00:bf:1f:2a:b8:cc:db:0d:53:
    06:4e:e0:b7:20:6a:10:22:bd:fb:03:26:42:83:b8:
    b9:f1:4f:af:e8:60:6b:73:68:aa:6e:86:53:22:d8:
    fc:52:26:ef:e0:00:14:c2:b9:a6:72:05:bc:c6:f3:
    7c:97:05:0e:45:c8:64:56:96:3b:51:b4:83:37:ee:
    aa:ca:a1:01:f4:e2:d3:1b:cc:a8:d9:fe:a9:11:63:
    35:e6:0b:56:0a:74:fb:44:9f:b4:b8:c3:d0:45:34:
    60:ad:bd:69:ca:fa:81:38:35:13:f2:68:a7:c8:e2:
    5b:b1:60:64:2f:72:7f:c7:c5:f8:9a:a2:c9:d7:11:
    3e:f1:42:ac:88:93:5c:f8:ab:f6:a5:59:6b:42:5a:
    1f:7e:9d:aa:02:fc:e9:a1:53:0a:91:a0:eb:a0:8a:
    d7:5d:7a:d4:72:44:b8:93:61:5c:51:cb:29:71:91:
    95
P:   
    00:e8:4b:4c:d9:14:19:57:0e:97:d5:58:bf:d0:68:
    76:c1:7c:85:e9:bd:34:d7:83:8b:47:fb:0a:75:42:
    82:92:5b:60:d8:a8:01:7e:16:be:0d:bb:af:95:e1:
    a6:2b:2b:62:05:a3:b8:47:cc:9d:87:55:99:e0:fb:
    9d:a8:d9:94:b3:cd:0b:09:22:07:d7:5c:15:a1:d7:
    c4:bc:44:cf:83:02:cb:af:ce:f9:21:77:b9:1f:2b:
    58:06:96:c2:0d:33:07:13:bb:18:c1:4e:dc:86:53:
    62:6a:ce:90:52:9c:41:58:1a:e8:11:da:5f:39:67:
    ba:a7:d7:cb:20:6f:92:61:c3:02:b4:73:c5:46:63:
    fd:11:47:ba:0e:e1:9e:35:bd:e2:e1:91:97:08:71:
    da:e3:9d:ed:76:20:da:16:4b:94:23:e7:0b:50:0a:
    4d:43:d6:f3:ad:82:a8:3b:07:53:82:ff:5c:f2:6a:
    43:dc:9b:4b:00:e9:59:37:9e:d2:6b:f2:17:ba:0a:
    79:78:8e:12:d8:31:ab:88:1b:2f:a2:bd:97:2e:a2:
    2b:ff:50:eb:6d:11:63:43:5f:6b:b8:8b:f4:cd:d5:
    b9:fe:72:99:c4:20:fc:41:76:b7:14:67:d4:27:94:
    15:18:66:51:06:a3:73:f5:02:f9:3e:8f:da:6f:15:
    97:67
G:    2 (0x2)
```

In the same way Bob:

```bash
bob@node2: ~$ openssl genpkey -paramfile dhparam.pem -out dhprivkey-bob.pem

bob@node2: ~$ openssl pkey -in dhprivkey-bob.pem -text -noout
DH Private-Key: (2048 bit)
private-key:
    52:e2:c6:a1:5f:58:b5:11:6d:04:ae:a2:af:f9:b1:
    37:96:7a:cb:59:f3:63:6c:02:a3:41:5b:d2:a8:d9:
    f0:cb:8b:b8:af:6a:a7:2d:32:10:87:19:cb:96:5e:
    d9:30:c7:c8:26:ca:e7:51:94:7a:01:79:0f:45:77:
    05:e9:d1:bb:86:de:16:4d:ab:03:f5:b7:ad:7b:0f:
    5b:ff:0a:31:78:be:b2:ae:1e:f8:f7:79:bd:40:b3:
    01:1f:4a:5e:91:57:35:94:66:90:09:5b:26:e4:73:
    ee:20:fc:da:12:53:3f:b1:01:b9:ac:e2:ed:03:9f:
    a0:70:9e:13:57:75:6d:25:29:7f:bd:53:93:5a:24:
    d5:5a:c6:98:0c:79:4e:e8:fb:a1:ce:2e:67:b6:f0:
    84:d8:ba:97:b3:e1:e8:18:1f:3a:b5:fc:97:dd:65:
    2a:a7:5a:a9:e9:12:d0:32:4a:1b:b4:74:4d:bc:90:
    e8:40:55:52:f8:2c:27:d4:1d:09:cd:57:66:a3:f3:
    79:fc:5f:99:f2:31:11:ae:25:92:cb:65:b6:ec:12:
    be:e7:ae:4b:ec:9d:e6:3f:28:fc:2a:37:a4:0f:11:
    4f:80:92:1d:c4:02:ff:f9:cc:21:b9:29:a3:f4:e2:
    75:f3:52:a7:a8:dc:7f:57:63:bd:d1:b2:79:9f:bf:
    06
public-key:
    00:bb:f0:e1:8c:88:8e:df:6d:43:f1:db:ae:c5:94:
    76:a9:65:41:af:dd:21:90:8d:3a:f9:7c:54:b7:f8:
    91:a7:49:cf:6c:9f:ba:7c:ad:37:62:a5:fd:59:0a:
    2a:ad:d3:b7:ef:4b:3c:d5:fc:23:6e:67:0e:88:5a:
    e0:90:1b:d7:21:31:d3:39:c7:7b:37:79:46:8d:8f:
    de:da:02:1b:1d:51:2f:39:ef:fb:4a:4e:90:ed:8d:
    c8:6d:a5:10:d0:62:79:c8:6f:84:1b:19:cc:0f:d9:
    aa:6f:87:df:61:99:9b:69:8c:00:b2:ab:b4:e2:94:
    68:4e:95:c2:99:c7:aa:31:61:9b:86:da:ee:ea:e3:
    00:ac:f6:fb:7f:97:2d:05:c8:af:3a:4f:52:40:f4:
    24:66:09:cc:ca:21:4f:9a:2a:ca:96:ca:7b:21:4d:
    de:32:1d:6e:6e:fa:ac:aa:16:a3:40:ba:25:6b:6a:
    7d:a5:03:b1:0f:47:06:5d:d0:d7:e7:60:a4:a3:f2:
    62:39:64:6a:44:1d:57:5c:5f:6c:fb:7c:bd:1b:9e:
    44:37:41:84:bd:50:5d:f2:fa:fe:72:d2:e9:09:a8:
    f1:9f:d3:c8:68:d8:ac:e7:58:b9:f6:19:5f:50:51:
    b2:08:21:8b:7e:f3:18:2d:84:6f:08:58:48:84:b6:
    26:c9
P:   
    00:e8:4b:4c:d9:14:19:57:0e:97:d5:58:bf:d0:68:
    76:c1:7c:85:e9:bd:34:d7:83:8b:47:fb:0a:75:42:
    82:92:5b:60:d8:a8:01:7e:16:be:0d:bb:af:95:e1:
    a6:2b:2b:62:05:a3:b8:47:cc:9d:87:55:99:e0:fb:
    9d:a8:d9:94:b3:cd:0b:09:22:07:d7:5c:15:a1:d7:
    c4:bc:44:cf:83:02:cb:af:ce:f9:21:77:b9:1f:2b:
    58:06:96:c2:0d:33:07:13:bb:18:c1:4e:dc:86:53:
    62:6a:ce:90:52:9c:41:58:1a:e8:11:da:5f:39:67:
    ba:a7:d7:cb:20:6f:92:61:c3:02:b4:73:c5:46:63:
    fd:11:47:ba:0e:e1:9e:35:bd:e2:e1:91:97:08:71:
    da:e3:9d:ed:76:20:da:16:4b:94:23:e7:0b:50:0a:
    4d:43:d6:f3:ad:82:a8:3b:07:53:82:ff:5c:f2:6a:
    43:dc:9b:4b:00:e9:59:37:9e:d2:6b:f2:17:ba:0a:
    79:78:8e:12:d8:31:ab:88:1b:2f:a2:bd:97:2e:a2:
    2b:ff:50:eb:6d:11:63:43:5f:6b:b8:8b:f4:cd:d5:
    b9:fe:72:99:c4:20:fc:41:76:b7:14:67:d4:27:94:
    15:18:66:51:06:a3:73:f5:02:f9:3e:8f:da:6f:15:
    97:67
G:    2 (0x2)
```

Now they will exchange public component

For Bob, we first extract the public component and we exchange it.

```bash
bob@node2:~$ openssl pkey -in dhprivkey-bob.pem -pubout -out dhpubkey-bob.pem

bob@node2:~$ openssl pkey -pubin -in dhpubkey-bob.pem -text
-----BEGIN PUBLIC KEY-----
MIICJTCCARcGCSqGSIb3DQEDATCCAQgCggEBAOhLTNkUGVcOl9VYv9BodsF8hem9
NNeDi0f7CnVCgpJbYNioAX4Wvg27r5XhpisrYgWjuEfMnYdVmeD7najZlLPNCwki
B9dcFaHXxLxEz4MCy6/O+SF3uR8rWAaWwg0zBxO7GMFO3IZTYmrOkFKcQVga6BHa
XzlnuqfXyyBvkmHDArRzxUZj/RFHug7hnjW94uGRlwhx2uOd7XYg2hZLlCPnC1AK
TUPW862CqDsHU4L/XPJqQ9ybSwDpWTee0mvyF7oKeXiOEtgxq4gbL6K9ly6iK/9Q
620RY0Nfa7iL9M3Vuf5ymcQg/EF2txRn1CeUFRhmUQajc/UC+T6P2m8Vl2cCAQID
ggEGAAKCAQEAu/DhjIiO321D8duuxZR2qWVBr90hkI06+XxUt/iRp0nPbJ+6fK03
YqX9WQoqrdO370s81fwjbmcOiFrgkBvXITHTOcd7N3lGjY/e2gIbHVEvOe/7Sk6Q
7Y3IbaUQ0GJ5yG+EGxnMD9mqb4ffYZmbaYwAsqu04pRoTpXCmceqMWGbhtru6uMA
rPb7f5ctBcivOk9SQPQkZgnMyiFPmirKlsp7IU3eMh1ubvqsqhajQLola2p9pQOx
D0cGXdDX52Cko/JiOWRqRB1XXF9s+3y9G55EN0GEvVBd8vr+ctLpCajxn9PIaNis
51i59hlfUFGyCCGLfvMYLYRvCFhIhLYmyQ==
-----END PUBLIC KEY-----
DH Public-Key: (2048 bit)
public-key:
    00:bb:f0:e1:8c:88:8e:df:6d:43:f1:db:ae:c5:94:
    76:a9:65:41:af:dd:21:90:8d:3a:f9:7c:54:b7:f8:
    91:a7:49:cf:6c:9f:ba:7c:ad:37:62:a5:fd:59:0a:
    2a:ad:d3:b7:ef:4b:3c:d5:fc:23:6e:67:0e:88:5a:
    e0:90:1b:d7:21:31:d3:39:c7:7b:37:79:46:8d:8f:
    de:da:02:1b:1d:51:2f:39:ef:fb:4a:4e:90:ed:8d:
    c8:6d:a5:10:d0:62:79:c8:6f:84:1b:19:cc:0f:d9:
    aa:6f:87:df:61:99:9b:69:8c:00:b2:ab:b4:e2:94:
    68:4e:95:c2:99:c7:aa:31:61:9b:86:da:ee:ea:e3:
    00:ac:f6:fb:7f:97:2d:05:c8:af:3a:4f:52:40:f4:
    24:66:09:cc:ca:21:4f:9a:2a:ca:96:ca:7b:21:4d:
    de:32:1d:6e:6e:fa:ac:aa:16:a3:40:ba:25:6b:6a:
    7d:a5:03:b1:0f:47:06:5d:d0:d7:e7:60:a4:a3:f2:
    62:39:64:6a:44:1d:57:5c:5f:6c:fb:7c:bd:1b:9e:
    44:37:41:84:bd:50:5d:f2:fa:fe:72:d2:e9:09:a8:
    f1:9f:d3:c8:68:d8:ac:e7:58:b9:f6:19:5f:50:51:
    b2:08:21:8b:7e:f3:18:2d:84:6f:08:58:48:84:b6:
    26:c9
P:   
    00:e8:4b:4c:d9:14:19:57:0e:97:d5:58:bf:d0:68:
    76:c1:7c:85:e9:bd:34:d7:83:8b:47:fb:0a:75:42:
    82:92:5b:60:d8:a8:01:7e:16:be:0d:bb:af:95:e1:
    a6:2b:2b:62:05:a3:b8:47:cc:9d:87:55:99:e0:fb:
    9d:a8:d9:94:b3:cd:0b:09:22:07:d7:5c:15:a1:d7:
    c4:bc:44:cf:83:02:cb:af:ce:f9:21:77:b9:1f:2b:
    58:06:96:c2:0d:33:07:13:bb:18:c1:4e:dc:86:53:
    62:6a:ce:90:52:9c:41:58:1a:e8:11:da:5f:39:67:
    ba:a7:d7:cb:20:6f:92:61:c3:02:b4:73:c5:46:63:
    fd:11:47:ba:0e:e1:9e:35:bd:e2:e1:91:97:08:71:
    da:e3:9d:ed:76:20:da:16:4b:94:23:e7:0b:50:0a:
    4d:43:d6:f3:ad:82:a8:3b:07:53:82:ff:5c:f2:6a:
    43:dc:9b:4b:00:e9:59:37:9e:d2:6b:f2:17:ba:0a:
    79:78:8e:12:d8:31:ab:88:1b:2f:a2:bd:97:2e:a2:
    2b:ff:50:eb:6d:11:63:43:5f:6b:b8:8b:f4:cd:d5:
    b9:fe:72:99:c4:20:fc:41:76:b7:14:67:d4:27:94:
    15:18:66:51:06:a3:73:f5:02:f9:3e:8f:da:6f:15:
    97:67
G:    2 (0x2)

```

In the same way for Alice:

```bash
alice@node1:~$ openssl pkey -in dhprivkey-alice.pem -pubout -out dhpubkey-alice.pem

alice@node1:~$ openssl pkey -pubin -in dhpubkey-alice.pem -text -noout
DH Public-Key: (2048 bit)
public-key:
    2e:27:2b:da:f1:98:17:ca:ed:c9:dd:6d:f7:56:7f:
    0f:6e:5b:2d:98:81:ef:cc:c6:bd:b0:b4:d4:e2:10:
    9c:2b:c1:34:be:ea:a3:a6:7c:7f:63:c9:11:66:5c:
    7e:21:b0:1e:a6:0b:e6:bf:e0:c4:e0:cf:dc:da:70:
    00:8b:4f:60:17:1e:b2:69:21:08:7d:e6:82:15:76:
    f5:96:ff:db:a9:12:00:bf:1f:2a:b8:cc:db:0d:53:
    06:4e:e0:b7:20:6a:10:22:bd:fb:03:26:42:83:b8:
    b9:f1:4f:af:e8:60:6b:73:68:aa:6e:86:53:22:d8:
    fc:52:26:ef:e0:00:14:c2:b9:a6:72:05:bc:c6:f3:
    7c:97:05:0e:45:c8:64:56:96:3b:51:b4:83:37:ee:
    aa:ca:a1:01:f4:e2:d3:1b:cc:a8:d9:fe:a9:11:63:
    35:e6:0b:56:0a:74:fb:44:9f:b4:b8:c3:d0:45:34:
    60:ad:bd:69:ca:fa:81:38:35:13:f2:68:a7:c8:e2:
    5b:b1:60:64:2f:72:7f:c7:c5:f8:9a:a2:c9:d7:11:
    3e:f1:42:ac:88:93:5c:f8:ab:f6:a5:59:6b:42:5a:
    1f:7e:9d:aa:02:fc:e9:a1:53:0a:91:a0:eb:a0:8a:
    d7:5d:7a:d4:72:44:b8:93:61:5c:51:cb:29:71:91:
    95
P:   
    00:e8:4b:4c:d9:14:19:57:0e:97:d5:58:bf:d0:68:
    76:c1:7c:85:e9:bd:34:d7:83:8b:47:fb:0a:75:42:
    82:92:5b:60:d8:a8:01:7e:16:be:0d:bb:af:95:e1:
    a6:2b:2b:62:05:a3:b8:47:cc:9d:87:55:99:e0:fb:
    9d:a8:d9:94:b3:cd:0b:09:22:07:d7:5c:15:a1:d7:
    c4:bc:44:cf:83:02:cb:af:ce:f9:21:77:b9:1f:2b:
    58:06:96:c2:0d:33:07:13:bb:18:c1:4e:dc:86:53:
    62:6a:ce:90:52:9c:41:58:1a:e8:11:da:5f:39:67:
    ba:a7:d7:cb:20:6f:92:61:c3:02:b4:73:c5:46:63:
    fd:11:47:ba:0e:e1:9e:35:bd:e2:e1:91:97:08:71:
    da:e3:9d:ed:76:20:da:16:4b:94:23:e7:0b:50:0a:
    4d:43:d6:f3:ad:82:a8:3b:07:53:82:ff:5c:f2:6a:
    43:dc:9b:4b:00:e9:59:37:9e:d2:6b:f2:17:ba:0a:
    79:78:8e:12:d8:31:ab:88:1b:2f:a2:bd:97:2e:a2:
    2b:ff:50:eb:6d:11:63:43:5f:6b:b8:8b:f4:cd:d5:
    b9:fe:72:99:c4:20:fc:41:76:b7:14:67:d4:27:94:
    15:18:66:51:06:a3:73:f5:02:f9:3e:8f:da:6f:15:
    97:67
G:    2 (0x2)
```

Note the public shared key is

```math
G^{privk} (mod p)
```


After key exchange, a common secret can be computed:

```bassh
alice@node1:~$ openssl pkeyutl -derive -inkey dhprivkey-alice.pem -peerkey dhpubkey-bob.pem -out secret-alice.bin
```

Now both keys are the same:
```
diff alice/secret-alice.bin bob/secret-bob.bin
```

There shall be no output.

## Perfomance Benchmarking

To compare perfomance on different algorithms:

```bash
$ openssl speed aes-128-cbc des md5
```

## Bibliography

PEM: Privacy Enhanced Mail  
DER: Distinguished Encoding Rules  
Then encrypt the message with Bob's public key  

FROM: https://sandilands.info/nsl/CryptographyinLinux.html  
https://www.geeksforgeeks.org/data-encryption-standard-des-set-1/  
https://security.stackexchange.com/questions/42046/what-do-the-dots-and-pluses-mean-when-openssl-generates-keys/140639#140639  
https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test  
https://www.thesslstore.com/blog/open-ssl-dots-pluses/  
https://www.di-mgt.com.au/crt_rsa.html  
https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Using_the_Chinese_remainder_algorithm  
