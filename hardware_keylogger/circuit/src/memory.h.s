.include "eeprom.h.s"

.globl memory_save_position			; 	- Save current position (16bit) to EEPROM (00h:00h)
.globl memory_persist_byte			;	- Transmit one character from circular buffer to EEPROM.
