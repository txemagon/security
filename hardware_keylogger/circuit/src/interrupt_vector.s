.include "interrupt_vector.h.s"

.AREA 	CODE 	(ABS)

; ------------------------------------------------------------------
; Push button interrupt

.ORG	0h0003
		ljmp	isr_pushbutton

; ------------------------------------------------------------------
; timer 0 interrupt
;
; Some timer information: https://www.electronicwings.com/8051/8051-timers

.ORG 	0h000B
		mov 	R7,#0
		clr 	TR0


; ------------------------------------------------------------------
; keyboard clock interrupt

.ORG 	0h0013
		ljmp  	isr_kbclk


; ------------------------------------------------------------------
; timer 1 interrupt
; Information on SFRs (in spanish): http://www.sc.ehu.es/sbweb/webcentro/automatica/web_8051/Contenido/tutor8051_52/Capitulo%202/descripcion_de_los_sfrs.htm
; http://www.sc.ehu.es/sbweb/webcentro/automatica/web_8051/Contenido/tutor8051_52/indicetutor_8051.htm
; http://www.sc.ehu.es/sbweb/webcentro/automatica/web_8051/Contenido/tutor8051_52/Capitulo%207/tabla_interrupciones.htm

.ORG 	0h001B
		inc 	DPTR
		reti

