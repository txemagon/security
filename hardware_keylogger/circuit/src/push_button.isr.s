.include "pushbutton.isr.h.s"

; Disconnect Keyboard before pressing the playback mode button!!

;---------------------------------------------------------------------
; service button press
isr_pushbutton::
pushbutton:

		mov  	IE,#0b00000000
		mov  	TCON,#0b00000100

		acall  	delay1s

		acall  	memory_save_position

		mov  	A,R5
		mov  	A,R7
		mov  	A,R6
		mov  	A,R3

		; ----------
		mov   	A,#EEMAX
		anl  	A,#0b11110000
		swap  	A
		acall 	dekod
		mov   	A,#EEMAX
		anl  	A,#0b00001111
		acall  	dekod

		; ----------
		mov  	A,R5
		anl  	A,#0b11110000
		swap  	A
		acall   dekod
		mov   	A,R5
		anl  	A,#0b00001111
		acall  	dekod

		mov  	A,R6
		anl  	A,#0b11110000
		swap  	A
		acall   dekod
		mov   	A,R6
		anl  	A,#0b00001111
		acall  	dekod

		; ----------

pet2:
		acall   get_ee

		mov  	R1,A
		anl  	A,#0b11110000
		swap  	A
		acall   dekod
		mov  	A,R1
		anl  	A,#0b00001111
		acall  	dekod

		jnb  	P3.2,pkon

		dec  	R3
		cjne  	R3,#255,nocarr
		dec  	R7

nocarr:	
		cjne  	R7,#0,pet2
		cjne  	R3,#0h7F,pet2

		mov  	R3,#255
		mov  	R7,#EEMAX
		dec  	R7
		ljmp  	pet2	

		; ----------

pkon:

		acall  	delay1s

		mov  	R0,#60
		mov  	R1,#60
		mov  	R2,#0
		mov  	R3,#0
		mov  	R4,#0
		mov  	R7,#0

		mov  	TCON,#0b00000100

		mov  	IE,#0b11001111
		mov  	IP,#0b00000100
		setb  	TR1

		reti