.include "ps2.h.s"

;---------------------------------------------------------------------
; PS2 Electrical Protocol:
;   https://www.youtube.com/watch?v=7aXbh9VUB3U
;
; PS2 Keyboard Protocol:
;   http://www-ug.eecg.toronto.edu/msl/nios_devices/datasheets/PS2%20Keyboard%20Protocol.htm
;
; KEYBOARD TO PC:
; | Initial bit | Data Byte | Parity bit | Final Bit |
;
; CLOCK FREQUENCY: 10kHz..30kHz -> T = 100us..33us
;
; PC TO KB:
;   PC holds clock down for a while.- CLK usually rests up.
;   When clock starts pulsing, PC sends data.
;---------------------------------------------------------------------
;---------------------------------------------------------------------
; PS2::kb_timeout
;
; 	- Wait 3 ms
ps2_kb_timeout:
		mov  	R2,#100
kbtm_1oop:
		acall  	delay30us
		dec  	R2
		cjne  	R2,#0,kbtm_1oop
		ret


;---------------------------------------------------------------------
; PS2::s1b
;
; 	- Send a logic one
;
; Set clock to upper state, loads data, wait 30us for the settling
; time, triggers a falling edge and raises the clock up again.
; 
; DESTROYS
;			R0
;			P3.3
;			P3.4

ps2_s1b::
		setb  	P3.3   			; clock = 1
		setb  	P3.4  			; data  = 1
		acall  	delay30us  		; settling time
		clr  	P3.3  			; Falling edge triggers reading
		acall   delay30us		; Reading time lapse
		setb  	P3.3 			; Reading ends

		ret



;---------------------------------------------------------------------
; PS2::s0b
;
; 	- Send a logic zero
;
; Set clock to upper state, loads data, wait 30us for the settling
; time, triggers a falling edge and raises the clock up again.
; 
; DESTROYS
;			R0
;			P3.3
;			P3.4

ps2_s0b:
		setb  	P3.3   			; clock = 1
		clr  	P3.4  			; data  = 0
		acall  	delay30us  		; settling time
		clr  	P3.3  			; Falling edge triggers reading
		acall   delay30us		; Reading time lapse
		setb  	P3.3 			; Reading ends

		ret

