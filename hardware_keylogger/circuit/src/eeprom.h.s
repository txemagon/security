; ------------------------------------------------------------------
;     MAX EEPROM ADDRESS
;
;     set this according to the used EEPROM chip - AT24C512
;	  Refer to AT24C512 for further information
;
; ------------------------------------------------------------------
; ------------------------------------------------------------------
;	  The AT24C512 provides:
;	
;	524,288 bits 	of electrically erasable and programmable
;					read only memory.
;
; 	65536   bytes
; 	64 		kB
;	2¹⁶		bytes
;
; 	Ergo, needs 2 bytes for addressing them.
; ------------------------------------------------------------------
; ------------------------------------------------------------------
;	EEMAX = 1			; 24C02
;	EEMAX = 2			; 24C04
;	EEMAX = 4			; 24C08
;	EEMAX = 8			; 24C16
;	EEMAX = 16			; 24C32
;	EEMAX = 32			; 24C64
;	EEMAX = 64			; 24C128
;	EEMAX = 128			; 24C256
	EEMAX = 0			; 24C512
;
; ------------------------------------------------------------------


; Simple functions
.globl eeprom_start_conditon		;	- Start condition for EEPROM
.globl eeprom_stop_conditon			; 	- Stop condition for EEPROM
.globl eeprom_acknowledge			;	- Send Output Acknowledge to EEPROM
.globl eeprom_acc2eeprom			; 	- Send acc to EEPROM


; Compound functions
.globl eeprom_init_reading			;	- Write device address for reading and acknowledge
.globl eeprom_init_writing			;	- Write device address for writing and acknowledge
.globl eeprom_stop_reading			;	- Send stop condition, device address for reading and wait read ack
.globl eeprom_stop_writing			;	- Send stop condition, device address for writing and wait write ack
.globl eeprom_read_eb				; 	- Read EEPROM character / byte and send stop condition
.globl eeprom_write_eb				;	- Write byte to EEPROM and acknowledge
