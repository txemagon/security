.include "kbclk.isr.h.s"
;---------------------------------------------------------------------
; PINOUT Info
;
; P3.3/INT1' 	PS2CLK
; P3.4/T0 		PS2DTA
; 
; PSW.5 		F0 - Programmer's flag
;
; Program Status Word Register
;   7   6   5   4   3   2   1   0 
;  CF  AC  F0  RS1 RS0 OF   -  PF
;
; F0: User defined flag
; OF: Overflow flag
; RS1-RS0: Register Bank from 0 to 3
;
;---------------------------------------------------------------------



;---------------------------------------------------------------------
; ISR::kbclk
;
; 	- Keyboard Clock Interrupt Service Routine
;
; Stores the newly arrived bit in R3 (temporary bit buffer)
; Whenever R7 is multiple of 8 it stores R3 (a whole byte) in 
; the circular buffer pointed by R1.
;
; Resets timer 0 on bit arrival and byte storage.
;
; INPUT:
; 			R7: 		Falling edges counter for INT1'
;
; DESTROYS: 
; 			A:			Used for paperwork
; 			R7:			Reset to 0 if more than 256 bits arrive
; 			TL0: 		Reset to 1
;			TR0: 		Updated for start / stop purposes
;
isr_kbclk::

		mov  	TL0,#1 					; Low part of timer 0 set to 1
										; Keyboard is sending, so we cannot send.


		;---------------------
		;    ENTRY PROTOCOL
		;---------------------
		mov  	PSW.5,C 				; Store the previous carry
		mov  	B,A  					; Store the previous accumulator



		;------------------------------------------
		; Check counter is not bigger than 8
		;------------------------------------------
		;------------------------------------------
		;
		;  Every 8 bits we shall complete a byte.
		;  And R7 counts the number of bits that
		;  already arrived.
		;------------------------------------------
		mov 	A,R7 					; if  (R7 % 8 != 0)
		dec     A                       ;
		anl  	A,#0b11111000           ;
		jnz  	inc_counter_and_exit 	; Start timer 0, inc R7 and move on.



		;------------------------------------------
		; Add the newly arrived bit to R3
		;------------------------------------------
		mov  	A,R3  					; R7 > 0 && R7 < 8
		mov  	C,P3.4 					; Read one bit and
		rrc  	A 						; from less significant bit to msb
		mov  	R3,A 			 		; store it in R3




 		; if (R7 != 8)
		cjne  	R7,#8,inc_counter_and_exit

		;------------------------------------------
		; Store whole byte in buffer
		;------------------------------------------
		; --- Increment head
		inc  	R1  					; Increase circular buffer summit
		cjne  	R1,#128,store_in_cb 	; And go all the way round in the range 3Ch:7Fh
		mov  	R1,#60 					; (44h - 68 bytes length) if upper limit is exceeded

		;--- And store
store_in_cb:
		mov  	@R1,A 					; Copy new character to buffer. R1 points to the last occupied position.
		mov  	TL0,#1  				; Restart clock
; 		set  	TR0


		;------------------------------------------
		; Count a new clock signal
		;------------------------------------------
inc_counter_and_exit:					; For the finishing leg
		setb  	TR0 					; Restore counter
		inc  	R7 						; And update our own personal counter R7

		;--  EXIT PROTOCOL
		mov  	A,B 					; Retore the previous accumulator
		mov  	C,PSW.5					; Restore the previous carry	

		reti