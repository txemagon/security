.include "memory.h.s"

;---------------------------------------------------------------------
; MEMORY ADDRESSES
;
; 	- 	A little bit of explanation taken out from the AT89 Series
;		Hardware Description.
;
; AT89C2051 bears:
;	- 256 RAM bytes.    	(0x100) addresses
;	- 2KB flash memory.		(0x800) addresses
;
; Special Function Registers (SFR) map in the range 80h - FFh.
; 
; Stack Pointer: 	8bit register
;					Initialized to 7Fh. First available location: 80h
;					Values are stored in little endian. (1) (8051 description)
; (1): 		AT89 Series Hardware Description stays that SP is initally
;			set to 07H, and so the first available position is 08h. That
;			actually neglects the description above. Truth must be sought,
;			at least, on a simulator. Further investigation lead me to 
; 			think sp dependes on the bank register activation.
;
; DTPTR 			16bit register = DTH + DTL
;
; MEMORY MAP
;   Internal Data Memory (00h:7Fh)
; 	  Register Bank Area
; 		00h:07h 		Register Bank 0
; 		08h:0Fh 		Register Bank 1  <- SP Points here initially, I read, but it doesn't seem trusty info.
; 		10h:18h 		Register Bank 2
; 		18h:1Fh 		Register Bank 3
;
; 	  Bit-addressable Area
; 		20h:2Fh
;
;	 Rest of RAM Area
; 		30h:7Fh			Variables growing from 30h, and
;						Stack growing downwards.
;
;	SFR
;		80h:FFh 		Special Function Registers
;
; FLASH
; From: https://www.keil.com/support/man/docs/is51/is51_ov_251arch.htm
; 		FFh:0000h		Code
;---------------------------------------------------------------------

.include "memory.h.s"

;---------------------------------------------------------------------
; MEMORY::persist_byte
;
; 	- Transmit one character from circular buffer to EEPROM.
;
; Uses internal eeprom autoupdated address register
;
; INPUT: 
; 			R0: 		Direction of [60 - 128] circular buffer in 
;						AT89C2051 Memory. R0 is supposed to have an 
;						initial value.
; 			R5 R6: 		EEPROM Address
;
; DESTROYS:
; 			R4: 		Set to 1
;			R5 R6:		Incremented
; 

memory_persist_byte::
persist_byte:
		inc  	R0
		cjne  	R0,#128,avoid_restart
		mov  	R0,#60

avoid_restart:
		acall  	eeprom_init_writing

		mov 	A,R5
		acall 	eeprom_write_eb
		mov  	A,R6
		acall   eeprom_write_eb

		mov  	A,@R0

;-------- debug --------------;
;                             ;
;      CJNE A,#0f8h,bbop      ;
;      ACALL changep17        ;
;                             ;
;bbop:                        ;
;-----------------------------;

		acall   eeprom_write_eb
		acall   eeprom_stop_condition


		; INC R5:R6
		inc  	R6 						; By incrementing R6


		cjne  	R6,#0,not_overflow 		; But if R6 overflows...
		inc  	R5 						; ...update R5
		cjne  	R5,#EEMAX,not_overflow  ; If R5 also overflows...
		mov  	R5,#0
		mov  	R6,#0h80				;... back to 0000:0080

not_overflow:
		acall  	wait_wr

		mov  	R4,#1 					; R4 marks the need to update R5:R6 pointer

		ret



;---------------------------------------------------------------------
; MEMORY::save_position
;
; 	- Save current position (16bit) to EEPROM (00h:00h)
; 
; At EEPROM [00h]: R5 R6 
; Clears R4 and DPTR
;
; INPUT:
; 				R5:R6 		- Address of circular buffer
;
; DESTROYS:
;				R4			- Set to 0
;				DPTR		- Set to 0
; 				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)

memory_save_position::
save_position:


		; -----------------               		Send 00 00 <= R5
		acall  	eeprom_init_writing
		
		mov  	A,#0  							; Set address
		acall   eeprom_write_eb
		mov  	A,#0
		acall   eeprom_write_eb
		
		mov  	A,R5
		acall   eeprom_write_eb					; Write data

		acall  	eeprom_stop_writing


		; -----------------               		Send 00 01 <= R6
		acall  	eeprom_init_writing

		mov  	A,#0  							; Set address
		acall   eeprom_write_eb
		mov  	A,#1
		acall   eeprom_write_eb
		
		mov  	A,R6   							; Write data
		acall   eeprom_write_eb

		acall  	eeprom_stop_writing

;--------  debug -------------;
;                             ;
;      ACALL changep16        ;
;                             ;
;-----------------------------;

		mov  	R4,#0
		mov  	DPTR,#0

		ret
