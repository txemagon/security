.include "eeprom.h.s"

;-----------------------------
;
; EEPROM AT24C512 FUNCTIONS
;


;---------------------------------------------------------------------
; EEPROM::start_condition 	
;				
;	-	Start condition for EEPROM
;
;
; A high-to-low transition of SDA with
; SCL high is a start condition which must precede any other
; command (refer to Start and Stop Definition timing dia-
; gram).
;
;
; INPUT:
;				-
; DESTROYS:
;				P3.5 (SCL)
;				P3.7 (DATA)
;
eeprom_start_condition::
start_condition:
		setb  	P3.7 					; data=1
		setb    P3.5  					; clk =1
		clr     p3.7 					; data=0!
		clr  	p3.5 					; clk =0
		ret



;---------------------------------------------------------------------
; EEPROM::stop_condition
;
; 	- Stop condition for EEPROM
; 
; A low-to-high transition of SDA with
; SCL high is a stop condition. After a read sequence, the
; stop command will place the EEPROM in a standby power
; mode (refer to Start and Stop Definition timing diagram).
;
;
; INPUT:
;				-
; DESTROYS:
;				P3.5 (SCL)
;				P3.7 (DATA)
;
eeprom_stop_condition::
stop_condition:
		clr  	P3.7					; data=0
		setb  	P3.5 					; clk =1
		setb    P3.7 					; data=1!
		clr   	P3.5 					; clk =0
		ret





;---------------------------------------------------------------------
; EEPROM::acknowledge
;
;	- Send Output Acknowledge to EEPROM.
; 
;	Defined in a diagram on page 6 of the AT24C512 manual.
;   Do not mistake sending ack TO EEPROM with the EEPROM ACK to controller.
;   (In the later a 0 is sent over then 9th clock cycle)
;   For the former CLK is switched up and down over a single sda high level,
;   which indeed is pretty similar to gathering start and stop conditions
;   together.
; 
; INPUT:
;				-
; DESTROYS:
;				P3.5 (SCL)
;				P3.7 (DATA)
;
eeprom_acknowledge::
acknowledge:
		setb  	P3.7	 				; data=1
		setb  	P3.5	 				; clk =1
		clr  	P3.5	   				; clk =0
		ret





;---------------------------------------------------------------------
; EEPROM::acc2eeprom
;
; 	- Send acc to EEPROM
;
; Send bit by bit all the bits in A to the EEPROM through the carry.
; The SDA pin is nor-
; mally pulled high with an external device. Data on the SDA
; pin may change only during SCL low time periods (refer to
; Data Validity timing diagram). Data changes during SCL
; high periods will indicate a start or stop condition as defined
; below.
;
;
; INPUT:
;				A  			- Accumulator. Character to be sent over the bus.
; DESTROYS:
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
;

eeprom_acc2eeprom::
acc2eeprom:
		mov  	R2,#8
dal:
		clr  	C  						; Erase carry
		rlc     A 						; Rotate left with carry
		mov  	P3.7,C 					; Send carry to EEPROM
		nop
daw:
		setb  	P3.5 					; clk =1
		nop
		clr  	P3.5 					; clk =0
		djnz  	R2,dal
		ret





;---------------------------------------------------------------------
;
; EEPROM:device_addr_w
; 	- Send device address word to write.
;
; Device address word structure
;		+---+---+---+---+---+----+----+------+
; 		| 1 | 0 | 1 | 0 | 0 | A1 | A0 | R/W' |
;		+---+---+---+---+---+----+----+------+
;
;	Mandatory Sequence 0f 1 & 0's
;	A1 A0: EEPROM Bus selection (Up to hardwired configured memories sharing the same bus)
;	R=1, W=0
;
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)

eeprom_device_addr_w::
device_addr_w:
		acall  	start_condition
		mov  	A,#0b10100000
		acall 	acc2eeprom
		ret




;---------------------------------------------------------------------
; EEPROM:device_addr_r
;
; 	- Send device address word to write.
;
; Device address word structure
;		+---+---+---+---+---+----+----+------+
; 		| 1 | 0 | 1 | 0 | 0 | A1 | A0 | R/W' |
;		+---+---+---+---+---+----+----+------+
;
;	Mandatory Sequence 0f 1 & 0's
;	A1 A0: EEPROM Bus selection (Up to hardwired configured memories sharing the same bus)
;	R=1, W=0
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)

device_addr_r:
		acall  	start_condition
		mov  	A,#0b10100001
		acall 	acc2eeprom
		ret





;---------------------------------------------------------------------
; EEPROM::wait_rd
;
; 	- Check if EEPROM ready for reading.
;
; Check if EEPROM sends Acknowledge after sending write device address
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)

eeprom_wait_rd::
wait_rd:
		acall  	device_addr_w 			; We say we want to write

		; Check P3.7 line response
		setb 	P3.7  					; Set data = 1
		setb  	P3.5  					; Rise the clock
		mov    	C,P3.7 					; If data still eq 1..
										; means the acknowledge (0) 
										; hasn't arrived on the 9yh cycle. So,

		clr  	P3.5  					; Lower the clock and ...
		jc  	wait_rd  				; repeat

		ret





;---------------------------------------------------------------------
; EEPROM::wait_wr
;
; 	- Check if EEPROM ready to write.
;
; Check if EEPROM sends Acknowledge after sending read device address
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)

eeprom_wait_wr::
wait_wr:
		acall  	device_addr_w

		; Load data and trigger edge
		setb  	P3.7 					; data=1
		setb  	P3.5 					; clk =1
		mov   	C,P3.7  				; Load data into carry
		clr  	P3.5  					; Trigger edge
		jc  	wait_wr
		ret




;---------------------------------------------------------------------
; EEPROM::write_eb
;
;	- Write byte to EEPROM and acknowledge
;
; Send Accumulator to EEPROM and Acknowledge
;
; INPUT:
;				A 			- Accumulator. Data to be sent.
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
eeprom_write_eb::
write_eb:
		acall 	acc2eeprom
		acall  	acknowledge
		ret




;---------------------------------------------------------------------
; EEPROM::eeprom_init_reading
;
;	- Write device address for reading and acknowledge
;
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
eeprom_init_reading::
		acall  	device_addr_r
		acall  	acknowledge
		ret




;---------------------------------------------------------------------
; EEPROM::eeprom_init_writing
;
;	- Write device address for writing and acknowledge
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
eeprom_init_writing::
		acall  	device_addr_w
		acall  	acknowledge
		ret



;---------------------------------------------------------------------
; EEPROM::eeprom_stop_reading
;
;	- Send stop condition, device address for reading and wait read ack
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
;

eeprom_stop_reading::
		acall  	stop_condition
		acall  	wait_rd
		ret




;---------------------------------------------------------------------
; EEPROM::eeprom_stop_writing
;
;	- Send stop condition, device address for writing and wait write ack
;
; DESTROYS:
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
;
eeprom_stop_writing::
		acall  	stop_condition
		acall  	wait_wr
		ret




;---------------------------------------------------------------------
; EEPROM::read_eb
;
; 	- Read EEPROM character / byte and send stop condition
;
;
; INPUT:
;				R3 			- Low Address &
;				R7			- High Address of reading starting address.
;
; DESTROYS:
;				C 			- Carry
;				A  			- Accumulator. Character to be sent over the bus.
; 				R2			- Used as a Bit counter
;				P3.5 		- (SCL)
;				P3.7 		- (DATA)
eeprom_read_eb::
read_eb:
		; Write R7 R3
		acall  	device_addr_w
		acall   acknowledge

		mov 	A,R7
		acall 	write_eb

		mov  	A,R3
		acall   write_eb

		; And start reading from EEPROM - READING PREPROTOCOL
		acall  	start_condition
		acall 	device_addr_r
		acall   acknowledge


		; Loop to acquire bits from EEPROM
		mov  	R2,#8  						; Number of bits still to read
		mov  	A,#0 						; Value already read. Initialization


read_edata:
		; Read one bit and set carry accordingly
		clr  	C  							; Clear Carry
		setb  	P3.5 						; clk = 1. Reading occurs while clk high.
		nop 								; 2 Spare cycles for setting time
		nop
		jnb 	P3.7,skip_set_c  					; if data != 1 => skip_set_c
		setb  	C  							; data == 1 => Set carry

skip_set_c:
		clr   	P3.5 						; clk = 0
		nop
		rlc 	A
		djnz   	R2,read_edata


		; End Reading - READING POSTPROTOCOL
		acall  	acknowledge
		acall  	stop_condition

		ret

