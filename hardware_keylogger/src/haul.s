.include "haul.h.s"

;---------------------------------------------------------------------
; HAUL::char_to_comp
; 
; - Transmit one character to computer.
;
; It does
;
; INPUT
;				A 		Byte to be send back to computer
; DESTROYS
; 				C
; 				A
;				R0
;				R4

haul_char_to_comp::



	; ----    Check Bus Status
		jb  	P3.3,int_free 			; If clk=1, bus iddles.

not_free:
		acall 	ps2_kb_timeout 			; Wait for 100 cicles (about 10 bytes).
		jnb  	P3.3,not_free 			; And check again.




	; ----    Bus Free
int_free:
		mov  	R4,#0
		acall 	ps2_s0b 				; Send Initial bit (0)



	; ----    Start Sending ACC bit by bit
		mov  	R2,#8 					; Set bit counter

nada:
		clr    	C
		rrc  	A  		 				; Send bits from LSB to MSB
		jnc  	ps2_s1b 				; through carry
		inc  	R4
		ljmp  	jedz2
odd:
		acall  	ps2_s1b
jedz2:
		acall   ps2_s1b

		mov  	R4,#60

sdelay2:
		acall  	delay30us
		djnz  	R4,sdelay2

		ret



;---------------------------------------------------------------------
; HAUL::Compose Char
;
; - Send Make and Release Scan Codes

haul_compose_char::
		mov  	DPTR,#0h350 			; Load the Translation Table Address
		movc 	A,@A+DPTR 				; A = Table[*A]
		mov  	B,A                     ; B = A
		acall  	haul_char_to_comp		; Send the MAKE Scan Code
		mov  	A,#0hF0					
		acall   haul_char_to_comp 		; Send RELEASE code
		mov  	A,B 
		acall  	haul_char_to_comp 		; Send Scan Code code
		ret


