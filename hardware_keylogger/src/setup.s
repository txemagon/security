.include "setup.h.s"

; A good memory map with the registers can be found at:
; https://www.electronicshub.org/8051-microcontroller-special-function-registers/

setup_initial::

		;--------------------
		;   INITIAL SETUP   ;
		; -------------------

		mov  	P3,#0hFF			;; Set High P3 an P1 pins
		mov  	P1,#0hFF

		acall  	delay1s

		; Work as timers (not counters) in auto reload mode
		; From: https://www.electronicwings.com/8051/8051-timers
		mov  	TMOD,#0b00100010 	;; TMOD register 0010 0010
									;; B7,3: Gate. 0 Enable timer/counter when TR0/TR1 is set
									;; T0 => clock. T1 => data
									;; B6,2: CTR/Timer'. 0 => Timer
									;; B[5:4],[1:0]: Mode. 2: 8 Bit autoreload mode
		

		; Counting starts at 1 for both timers
		mov  	TH1,#1
		mov  	TH0,#1

		
		; Timers stopped
		; Timer 1 will trigger on falling edges
		; Timer 0 will trigger at low levels
		; Interrupt 1 flag cleared by the software
		mov     TCON,#0b00000100  	;; Timer Control Register - 88H
									;; TF1 TR1 TF0 TR0 IE1 IT1 IE0 IT0
									;; TFx: Overflow flag of timer x. Cleared after interrupt attended.
									;; TRx = "1" allows counting. 0 means: do not count
									;; IEx: External interrupt requested. Automatically cleared after being attended.
									;; ITx: 1 => falling edge. 0 => low level.
									;; Johan: Timer parados, T1 activado por flanco de bajada.
		;; Please look carefully at figure 9, page 2-46, of the "AT89 Series Hardware Description".
		;; As seen there INT0' Pin will trigger the counter when the gate is low.

		; And: http://www.sc.ehu.es/sbweb/webcentro/automatica/web_8051/Contenido/tutor8051_52/Capitulo%202/descripcion_de_los_sfrs.htm
		mov  	IE,#0b11001111  	;; Interrupt Enable 0A8H
									;; B7 - EA: EA = 1. Interrupts enabled
									;; B4 - ES: Serial port interrupt mask
									;; B3 - ET1: Timer 1 interrupt enabled
									;; B2,0 - EXx: External interrupt 1 and 0 enabled. 
									;; B1 - ET0: Timer 0 interrupt enabled

		mov  	IP,#0b00000100		;; Interrupt Priority, 0B8H
									;; X X X PS PT1 PX1 PT0  PX0
									;; PS: Serial Port
									;; PXx: External
									;; PTx: Timer
									;; 1: High
								    ;; 0: Low
								    ;; External 1 => High priority

		clr  	TR0
		clr  	TR1

		mov  	P3,#0hFF
		mov  	P1,#0hFF

		clr     P3.5  				;; Connected to AT24C512 EEPROM SCL clock signal

		ret