.include "delays.h.s"

.globl ps2_kb_timeout	; 	- Wait 10 chars and check if bus is free
.globl ps2_s0b			; 	- Send a logic zero
.globl ps2_s1b			; 	- Send a logic one