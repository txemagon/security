.include "delays.h.s"

;-----------------
;
;   DLEAY SUBROUTINES   
;

;---------------------------------------------------------------------
; DELAY::delay1s
;
;	- delay 1s
;
; Theoretically time is 658454MC * 1us/MC = 658ms (not 1s)
;
; DESTROYS:
; 				R0
;				R3
;				R7

delay1s::
		mov  	R0,#0H05   		;; 1 x 2 = 2
ydelay1:
		mov  	R7,#0HFF		;; 5 x 2 = 10
ydelay2:
		mov  	R3,#0HFF		;; 256 x 2 = 512 MC 
ydelay3:
		djnz    R3,ydelay3   	;; 5 * 0x100 * 0x100 = 50000 	(327680)
		djnz    R7,ydelay2   	;; 5 * 0x100 = 0x500 			  (1280)
		djnz    R0,ydelay1		;; 5                                 (5)
								;;                          2 x (328965)  Instructtions
								;;								(657930)  Machine Cycles
								;; Total = 	657930 + 524 = 658454 MC

		ret



;---------------------------------------------------------------------
; DELAY::delay200ms
; 
;	- delay 200ms
;
; DESTROYS:
;				R3
;				R7

delay200ms::
		mov  	R7,#0HFF
zdelay2:
		mov    	R3,#0HFF
zdelay3:
 		djnz  	R3, zdelay3
 		djnz    R7, zdelay2

 		ret




;---------------------------------------------------------------------
; DELAY::delay30us 
; 
;	- delay 30us 
;
; DESTROYS:
; 				R0


delay30us::  
      	mov 		R0,#10
delay1: 
		dec 		R0           
        cjne 		R0,#0,delay1

       	ret

