# RSA

## Bases Teóricas

Contando con que lo que es un resto es conocido y la aritmética modular también,
algunos hecho interesantes de los que nos hemos ido dando cuenta a lo largo de la historia son...

### Euler y Fermat

... por ejemplo que:

| Base   | Exponente | Potencia                  | Operación                            |
|:------:|:---------:|---------------------------|--------------------------------------|
| ${2}$  | ${3}$     | ${2^3 = 8}$               | ${2^3 \equiv 2 \quad (mod \ 3)}$     | 
| ${2}$  | ${7}$     | ${2^7 = 128}$             | ${2^7 \equiv 2 \quad (mod \ 7)}$     | 
| ${7}$  | ${13}$    | ${7^{13} = 96889010407 }$ | ${7^{13} \equiv 7 \quad (mod \ 13)}$ | 

que tenía muy buena pinta, pero que en algunos casos,

| Base   | Exponente | Potencia                  | Operación                            |
|:------:|:---------:|---------------------------|--------------------------------------|
| ${3}$  | ${18}$     | ${3^{18} = 387420489}$   | ${3^{18} \equiv 9 \quad (mod \ 18)}$ | 

Y esos casos en los que no funcionaba eran, y son, cuando el exponente no es primo.


#### El Pequeño Teorema de Fermat

Así que Fermat lo puso por escrito de la siguiente forma:

```math
a^p \equiv a \quad (mod \ p), \forall a>0 \iff p \in \{ primos \}
```

Es lo que hay, se escribe raro, pero con el ejemplo se entiende más que de sobra.

#### EL Teorema de Euler

Si dividimos lo anterior por ${ a }$,

```math
a^{p-1} \equiv 1 \quad (mod \ p)
```

Y además se puede generalizar un poco más si ${p}$ no es primo usando la función totiente que ya hemos explicado en clase.

```math
a^{\phi(p)} \equiv 1 \quad (mod \ p)
```

Como ${n}$ no es primo, el totiente de ${n}$ se calcula como:

```math
\phi(n) = mcm( \phi(p), \phi(q) ) = \frac{\phi(p) \cdot \phi(q)}{ mcd(\phi(p), \phi(q)) }
```


```math
\begin{matrix}
p = 11 \\
q = 17 \\
n = p \cdot q & = 187 \\
\phi(p) = 10 \\
\phi(q) = 16 \\
\phi(n) = \frac {\phi(p) \cdot \phi(q)}{mcd( \phi(p), \phi(q) )} & = 80
\end{matrix} \\
```

## El algoritmo RSA

### Conceptualización

Sea un número ${n}$ compuesto por dos factores: ${ n = p \cdot q }$.
Sea un número ${m}$ que queremos cifrar.

Según el teorema de Euler,

```math
m^{\phi(n)} \equiv 1 \quad (mod \ n)
```

Y si lo multiplicamos todo por ${m}$,

```math
m^{\phi(n) + 1} \equiv m \quad (mod \ n)
```

O sea, que esta operación no altera el mensaje.

Si la pudieramos hacer en dos pasos, al primero le llamaríamos _cifrar_,
y al segundo _descifrar_.


### El Reto

Tenemos que encontrar dos numeros, ${d}$ y ${e}$ tales que:

```math
d \cdot e = k \cdot \phi(n) + 1 \\
d \cdot e \equiv 1 \quad mod( \phi(n) ) \\

```

### Cifrar y Descifrar

Así, si el mensaje ${ m = 6 }$, ${ p=11 }$ y ${ q = 13 }$,

entonces ${ n = p \cdot q = 143 }$,

y el totiente: ${ \phi(n) = \phi(p) \cdot \phi(q) = 120 }$

Debemos encontrar dos claves ahora ${d}$ y ${e}$ tales que ${ d \cdot e \equiv 1 \quad (mod \  120) }$.

Vamos a tomar por ejemplo ${ d=7 }$, entonces ${ e=103 }$, ya que 

```math
103 \cdot 7 = 721 \\
120 \cdot 6 = 720 \\
103 \cdot 7 \equiv 1 \quad (mod \  120) \\
```

#### Cifrando

Con la clave ${ d = 7 }$,

```math
m = 6 \\
6^7 = 279936 \\
6^7 \equiv 85 \quad (mod \ 143) \\
c = 85 \\
```

#### Descifrando

Con la clave ${ e = 41 }$,

```math
c = 85 \\
85^{103} = uufffff \\
85^{103} \equiv 6 \quad (mod \ 143) \\
m = 6 \\
```


