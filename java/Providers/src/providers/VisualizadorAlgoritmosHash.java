package providers;

import java.security.MessageDigest;
import java.security.Provider;
import java.security.Provider.Service;
import java.security.Security;
import java.util.Set;

public class VisualizadorAlgoritmosHash {

	public static void main(String[] args) {
		final String TIPO_MESSAGE_DIGEST = MessageDigest.class.getSimpleName();
		
		Provider[] proveedores = Security.getProviders();
		for (Provider proveedor : proveedores) {
			Set<Service> servicios = proveedor.getServices();
			for (Service servicio : servicios) {
				if (servicio.getType().equals(TIPO_MESSAGE_DIGEST)) {
					System.out.println(servicio.getAlgorithm());
			    }
		    }
		}
	}

}
