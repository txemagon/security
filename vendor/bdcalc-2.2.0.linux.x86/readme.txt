This is a legal document. Please read these terms carefully before
installing the Software as installation will indicate your acceptance
to all of the terms of this licence agreement.

IF YOU DO NOT AGREE TO THE TERMS, DO NOT INSTALL THE SOFTWARE.

`bdcalc` is an executable application provided free of charge. 
You may use it on your own computer for personal or business use, but 
you may NOT distribute, sell or claim ownership of this application. 
Install and use entirely at your own risk.
David Ireland and D.I. Management Services Pty Limited ("the Author") make no
representations concerning either the merchantability of the Software or its
suitability for any particular purpose. It is provided "as is" without express
or implied warranty of any kind. No warranty  of fitness for a particular
purpose is offered. In no event shall the Author be liable for any damages in
connection with or arising out of the use of the Software by any person
whatsoever, including incidental, indirect, special or consequential damages, or
any damages related to loss of use, revenue or profits, even if the Author has
been advised of the possibility of such damages. By using the Software you
expressly agree to such a waiver.

<http://www.di-mgt.com.au/contact/>

This doc last updated: 17 June 2016
+++++++++++++++++++++++++++++++++++
