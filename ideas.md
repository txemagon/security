# Cryptography

What to tell as an introduction

## Encryption Algorithms

### Symmetric Algorithm

- Data Encryption Standard (DES)
- Triple DES
- Advanced Encryption Standard (AES)
    - Key size 128 bit
    
### Asymmetric Algorithm

- RSA (Rivest-Shamir-Adleman)
    - Key size 1024, 2048, 3072, 4096,...

### Hash Algorithm

- MD5: Message Digest v5. Hash Length 128 bit
- SHA: Secure Hash Algorithm. Hash Length 160 bit
- SHA-2: Hash Length 224, 25, 384, 512
- RIPEMD: Raise Integrity Primitives Evaluation Message Digest. Hash Length 128, 160, 256, 320


Hashed Message Authentication Code (HMAC)
Alice:
1. Shared key (A&B) -> message.
1. Hash message and shared key to a file.
1. Send file and encrypted message

Bob:
1. Hash encrypted message and his own key.
1. Check hash against sended file.





## Digital Signatures

The sender encrypts a message using his private key



## Digital Certificates

Prevents man in the middle attacks
Certifcate Authority (CA): 
- Generate, issue and distribute public key certificates
- Distribute CA certificates
- Generate and publish certificate status information
- Revoke public key certificates


1. Alice wants a certificate and sends to CA Website, Business name, city, tate, Country, Email Address. And provides passport or any other identity.

1. CA generates certificate with:
- Name
- Public key
- Name of issuer
- Digital Signature of issuer
- Serial Number
- Expiration Date

1. Verification of the certificate
- Inside the certificate there is a summary Hash signed with CApk
- Bob makes a hash of the certificate and check against the decryption with CAPK of CA hash inside the certificate.


## Managing Digital Certificates

- Many Root CA: Bottlenecks, compromising the roor compromises the secondary CA
- Intermediate CA: Process and verify ID
- Registration Authorities RA: Identify and authentify requesters, verify public key.
- Certificate Repository (CR): Access to certificates and status ( expiration, Certificate Revocation, ...)

Cerificate Revocation:
- Cerificate Revocation List (CRL) {mmc.exe}
- Online Certificate Status Protocol (OCSP Responder)

## Types of Digital Certificates

On Certificate request to RootCA or IntermediateCA:

- Alice provides data to the CA for the certificate
- CA creates the file, and a hash of the file. Then cyphers the hash with the private key.

### Root Certificates:
- RCA certificates -> self signed
- ICA certificates -> RCA signed

### Domain Digital Certificates:

- Ensure the authenticity of the web servers to the client
- Ensure the authenticity of the cryptography:

Types:
1. Domain Validation: Verify the entity that has control over de domain. Verifies that an organization has the right to use the domain.
1. Extended Validation: Also validates the legitimacy of the businesss (physically). Lock + Name of the company in browser
1. Wildcard: Validates main domain and subdomains *.google.com
1. Subject Alternative Names (SAN): Multiple domains with the same certificate

### Hardware & Software Digital Certificates

1. Machine Digital Certificate: Verify ID of a device on a network
1. Code Signing Digital Certificate: 
1. Email Digital Certificate: 


## Public Key Infrastructure (PKI)

Authorities:
- Root
- Intermediate
- Registration

Alice -> aws
Bob -> palo alto networks

### Trust Model

1. Hierarchical Trust Model
1. Distributed Trust Model
1. Bridge Trust Model

#### Hierarchical Trust Model

Root (Master Certification Authority) issues both certificates.
Only used at company level.

#### Distributed Trust Model

Multiple CA. But this authorities have a certificate issued by a single Master CA. Receiver validates sender's ICA via de Root CA.

#### Bridge Trust Model

A bridge facility recognises different RCAs.


### Policies

#### Certificate Policy (CP)

A set of rules to govern PKI

* Operation of PKI
* Baseline security requirement
* CA obligations
* Users Obligations

#### Certificate Practice Statements (CPS)

A technical document that specifies:

1. How CA uses & manages certificate
1. How to register for digital certificate
1. How to issue digital certificate
1. When to revoke digital certificate

### Certificate Stages

1. Creation
1. Suspension
1. Revocation
1. Expiration

### Key Management

There are procedures for:

1. Storage
1. Usage
1. Handling


## Diffie-Hellman Key Exchange


An algorithm for key exchange.

Two large prime numbers: n, g.

Alice decides a prime number x
Bob decides a prime number y

### Example
```
Alice              Public         Bob
-----              ------         ------
# Agreement on public numbers
                   n = 11
                   g = 7

# Choosing private keys             
 x = 3                            y = 5
   |                                |
   V                                V
 A = g^x mod n                    B = g^y   mod n
 A = 7^3 mod 11                   B = 7^5   mod 11
 A = 343 mod 11                   B = 16807 mod 11
 A = 2                            B = 10


# Sending an intermediate public result 
   Send A  ----- public 2 ------> A = 2
 B = 10    <---- public 10 ------ Send B
 
# Calculating a symmetric key
 K = B^x  mod n                   K = A^x mod n
 K = 10^6 mod 11                  K = 2^5 mod 11
 K = 10                           K = 10
 
```

### Observations

- It doesn't prevent man in the middle attacks

### Commands

```bash
# Both deprecated
dh
dhgen 
```

With Openssl

Generator of 2 and a random 1024-bit prime number.
```bash
openssl dhparam -out dhparam.pem -2 1024 
```

If you want to see the numbers:
```bash
openssl dhparam -in dhparam.pem -noout -text
```

There used to be a -C option that created the params in C code


Lets have a look at the generated certicate:

```bash
sed '1d;$d' dhparam.pem | base64 -d | od -tx1
```

```
0000000 30 81 87 02 81 81 00 be 6f ca 5d 95 e4 c5 0b ea
0000020 29 f7 df 37 aa 10 0c 71 51 04 01 67 b2 aa 2b 68
0000040 80 1e d7 48 35 74 2b 4d 44 09 95 36 d8 6b 6d 20
0000060 66 11 c1 ae 1b 6a 7d e7 77 23 5b 7a 86 a7 2b 92
0000100 f1 cd 80 de d0 19 0a 51 af 74 c8 82 c8 44 bb f2
0000120 15 da 9f e4 cf b2 4c a9 38 90 d0 ee eb 63 37 ab
0000140 f6 e7 6c 2b c4 04 e2 76 64 0f cd f2 86 30 98 e4
0000160 1b 57 b1 f4 18 d9 97 8b 24 c8 55 b3 36 7a 4a e2
0000200 10 ab db 14 aa 61 0f 02 01 02
0000212
```

```bash
openssl dhparam -in dhparam.pem -noout -text
```

yields,

```
    DH Parameters: (1024 bit)
    P:   
        00:be:6f:ca:5d:95:e4:c5:0b:ea:29:f7:df:37:aa:
        10:0c:71:51:04:01:67:b2:aa:2b:68:80:1e:d7:48:
        35:74:2b:4d:44:09:95:36:d8:6b:6d:20:66:11:c1:
        ae:1b:6a:7d:e7:77:23:5b:7a:86:a7:2b:92:f1:cd:
        80:de:d0:19:0a:51:af:74:c8:82:c8:44:bb:f2:15:
        da:9f:e4:cf:b2:4c:a9:38:90:d0:ee:eb:63:37:ab:
        f6:e7:6c:2b:c4:04:e2:76:64:0f:cd:f2:86:30:98:
        e4:1b:57:b1:f4:18:d9:97:8b:24:c8:55:b3:36:7a:
        4a:e2:10:ab:db:14:aa:61:0f
    G:    2 (0x2)
```

Note P can be found at offset 06.

## SSL / TLS Communication

SSL v3: 
- safe nowadays (2023)
- basis for TSL 1.0

TLS is safer.

https: Port 443

### Handshake

```
Client             Server
------             -------

 ---- Client Hello --->
 <--- Server Hello ----
 <--------------------- Certificate, Server Key Exchange, Server Hello DOne
 ---------------------> Client Key Exchange, Change Cipher Spec, Encrypted Handshake Message
 <--------------------- Change Cipher Spec
 <--------------------- Encrypted Handshake Message
 
 <---------------------
 --------------------->
```

```
           ┌──────────┐                                    ┌──────────┐
           │  CLIENT  │                                    │  SERVER  │
           └──────────┘                                    └──────────┘

      ┌──────────────────────┐                                  │
      │     CLIENT HELLO     │                                  │
      ├──────────────────────┤                                  │
      │ SSL Protocol Version │                                  │
      │       Random         ├──────────────────────────────────┤
      │     Session ID       │                                  │
      │    Cypher Suite      │                                  │
      │                      │                                  │
      └──────────┬───────────┘                   ┌──────────────┴────────────────┐
                 │                               │         Server Hello          │
                 │                               ├───────────────────────────────┤
                 │                               │      SSL Protocol Version     │                       S
                 │                               │      Random, Session ID       │
                 │◄──────────────────────────────┤        Cypher Suite           │
                 │                               │       Compression Method      │
                 │                               │Digital Certificate (Public Key│
                 │                               │                               │
                 │                               └──────────────┬────────────────┘
                 │                                              │
                 │                               ┌──────────────┴────────────────┐
                 │                               │       Server Key Exchange     │
                 │                               ├───────────────────────────────┤
                 │                               │          DH Parameters        │
                 │                               │          Hello Done           │
                 │                               └──────────────┬────────────────┘
                 │                                              │
                 │                                              │
                 │                                              │
                 │                                              │
  ┌──────────────┴─────────┐                                    │
  │ Certificate Validation │                                    │
  └──────────────┬─────────┘                                    │
                 │                                              │
                 │                                              │
   ┌─────────────┴─────────┐                                    │
   │ CLIENT KEY EXCHANGE   │                                    │
   │ {Premaster Key}SPK    ├───────────────────────────────────►│
   └────────────┬──────────┘                                    │
                │                                               │
                │                                               │
                │                                               │
┌───────────────┴───────────────┐                ┌──────────────┴────────────────┐
│ Generate Premaster Secret Key │                │ Generate Premaster Secret Key │
└───────────────┬───────────────┘                └──────────────┬────────────────┘
                │                                               │
                │                                               │
   ┌────────────┴────────┐                                      │
   │  Change Cypher Spec ├─────────────────────────────────────►│
   └────────────┬────────┘                                      │
                │                                               │
                │                                               │
    ┌───────────┴────────┐                                      │
    │ Handshake Finished ├─────────────────────────────────────►│
    └───────────┬────────┘                                      │
                │                                   ┌───────────┴────────┐
                │◄──────────────────────────────────┤ Change Cypher Spec │
                │                                   └───────────┬────────┘
                │                                               │
                │                                               │
                │                                   ┌───────────┴────────┐
                │◄──────────────────────────────────┤ Handshake Finished │
                │                                   └────────────────────┘
                │
```

Client Hello Session ID = 0 at the beginning

```
TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 (0xc02b) 
```

Key exchange with Diffie-Hellman
Symmetric encryption: AES 128
Hashing Algorithm: SHA-256

Certificate is a list of the certification chain

For delving into wireshark: https://www.youtube.com/watch?v=sjC5DS4jf_k&list=PL7d8iOq_0_CWAfs_z4oQnCuVc6yr7W5Fp&index=8


## SSH Protocol

```
┌──────────────────────────────┬────────────────────────────┐
│                              │                            │
│    SSH User Authentication   │ SSH Connection Protocol    │
│                              │                            │
├──────────────────────────────┴────────────────────────────┤
│                 SSH Transport Layer Protocol              │
│                                                           │
├───────────────────────────────────────────────────────────┤
│                              TCP                          │
│                                                           │
└───────────────────────────────────────────────────────────┘
```

- SSH Connection Protocol: Multiplexes multiple channels over a sinble SSH connection.
- SSH User Authentication: User authentication
- SSH Transport Layer Protocol: Server Authentication and Data confidentiality
- TCP

One channel for session, another for port forwarding,...

TCP Handshake client & Server:
```
syn -->
<-- SYN/ACK
ACK -->
```

1. Identification String exchange: {SSH-Protocolo Version, Software Version} <-->
1. Algorithm Negotiation {KEXINIT: Encryption, KeyX, Message Auth., Compression} --> & KEXINIT <--
1. Key Exchange. (DH ECDH)
    1. Client: Key Pair generation for Diffie-Hellman
    1. Client: Key sending `SSH_MSG_KEX_ECDH_INIT`
    1. Server: Diffie-Hellman key generation.
    1. Server: Compute Secret Key and exchange Hash{Client ID String, Server ID String, CLient KEXINIT, Server KEXINIT, Server Public Key, Client Ephemeral DH Public Key, Server Ephemeral DH Public Key, Shared Secret key}Server Private Key. The Hash is the session key
    1. `SSH_MSG_KEX_ECDH_REPLY`: HASH, Host Public Key, DH Key
    1- Client: Compute HASH
    1. Verify of accept public key
1. DH Key derives 6 new keys: (encryption, initializat_REQUEST`ion vectors(IV), Integrity) `SSH_MSG_NEWKEYS`
1. `SSH_MSG_SERVICE_REQUEST`
1. `SSH_MSG_SERVICE_ACCEPT`
    
## References:
 
CBTVid: https://www.youtube.com/watch?v=k3uFPGieZS8&list=PL7d8iOq_0_CWAfs_z4oQnCuVc6yr7W5Fp&index=5

Elliptic Curves Differences: https://security.stackexchange.com/questions/50878/ecdsa-vs-ecdh-vs-ed25519-vs-curve25519

Certificates: https://www.ssl.com/guide/pem-der-crt-and-cer-x-509-encodings-and-conversions/

