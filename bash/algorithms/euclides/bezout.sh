# Bezout coefficients Library
#
# Given a list of the euclidean coefficients algorithms: q0 q1 q2 .. qn
# figure out a and b such: ax + by = 1
#
# Inspired on https://www.youtube.com/watch?v=D-ATs6TyYWA
# @export bcoef q0 q1 . . . qn


#####################
# A LITTLE REVIEW
#####################


# THE EUCLIDEAN ALGORITHM ...
# a  = q0·b  + r0
# b  = q1·r0 + r1
# r0 = q2·r1 + r2

# Can be written as a sequence of matrices ...
#
#  /       \   /       \           /      \     /    \
# |  q0  1  | |  q1  1  |         | qn  1  |   | α  β |
# |  1   0  | |  1   0  | . . . . | 1   0  | = | γ  δ |
#  \       /   \       /           \      /     \    /
#

# Each of them with det = -1

# That can be summarized ...
#
#  /   \       /    \   /          \
# |  a  |     | α  β | |  gcd(a,b)  |
# |  b  |   = | γ  δ | |     0      |
#  \   /       \    /   \          /
#


# And inverted:
#
#  /          \                /      \   /   \
# |  gcd(a,b)  |              |  δ  -β | |  a  |
# |     0      | = (-1)^(n+1) | -γ   α | |  b  |
#  \          /                \      /   \   /
#

# So:
#
# gcd(a,b) = (-1)^(n+1) [  δ·a -  β·b ]


# HANDS ON...

# Multiply a row by a column
function rbc(){
    a11=$1; a12=$2
    b11=$3; b21=$4

    echo -n $(( $a11 * $b11 + $a12 * $b21 ))
}

# 2D matrices multiplication. Write down sequentially every row of the first matrix and then the second one.
function mul_mat() {
    a11=$1; a12=$2; a21=$3; a22=$4
    b11=$5; b12=$6; b21=$7; b22=$8

    echo -n $(rbc $a11 $a12 $b11 $b21) $(rbc $a11 $a12 $b12 $b22) " "
    echo -n $(rbc $a21 $a22 $b11 $b21) $(rbc $a21 $a22 $b12 $b22) " "
    echo

}

# Build a bezout matrix: q 1 1 0
function bm() {
    echo $1 1 1 0
}

function bcoef() {
    read -a qarr <<<"$@"
    # Get the sign of the determinant
    off=0
    [[ $(( ${#qarr[@]} % 2 )) -ne 0  ]] && off=1

    r="1 0 0 1"
    for i in "${!qarr[@]}"; do
        j=$(( ${#qarr[@]} - i - 1 ))
        r="$( mul_mat $(bm "${qarr[j]}") $r )"
    done

    # Uncomment if you want to know the summary matrix
    # echo $r >&2

    a=$( echo -n $r | cut -f4 -d" " )
    b=$( echo -n $r | cut -f2 -d" " )

    sign=( "" "-" )
    echo "${sign[$(( $off ))]}$a ${sign[$(( ($off+1) % 2 ))]}$b"

}
