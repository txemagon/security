# Library for handling hexadecimal inputs or outputs
#
# @export normbase to convert the input in the given format.
# @export print
# Define a function parse_extra to process extra parameters.
# txemagon, 2023

#########################################
# ANALYSING THE COMMAND LINE ARGUMENTS

extra_opts=${extra_opts:-}
conversion=0
# Parsing the command line with getopts
while getopts "${extra_opts}hx:" OPTION; do
    case $OPTION in
        x)
            offset=$OPTARG
            case $OPTARG in
                i)
                    conversion=1
                    ;;
                o)
                    conversion=2
                    ;;
                e)
                    conversion=3
                    ;;
                *)
                    usage "Only the input (i), the output(o), or everything(e) can be converted to hex !"
                    exit 2
                    ;;
            esac
            ;;
        h)
            usage "Here is your help"
            exit 0
            ;;
        *)
            if [[ $extra_opts =~ $OPTION ]]; then
                if [[ $(type -t parse_extra) == function ]]; then
                    parse_extra $OPTION $OPTARG
                    if [[ $? -ne 0 ]]; then
                        usage "Error processing parameter: $1" >&2
                        exit 2
                    fi
                else
                    usage "Cannot parse parameter: $1" >&2
                    exit 1
                fi
            else
                usage "Unknown parameter: $1" >&2
                exit 1
            fi
            ;;
    esac
done

shift $((OPTIND - 1))

# Was data specified in hex?
function normbase() {
    if [[ $(($conversion % 2)) -eq 1 ]]; then
        echo $(( 16#$1 ))
    else
        echo $1
    fi
}


print='printf %d'
if [[ $conversion -gt 1 ]]; then
    print='printf %X'
fi


