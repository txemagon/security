// ES7 power and multiply modulo
// Use it inside Apps Script to enhance google sheets with PBSMM function

/* Auxiliar Functions */
// Modulo alternative
function prune(figure, modulo) {
  return figure > modulo ? figure % modulo : figure
}

// Binary Stack
function binary_stack(n) {
  let stack = []

  if (n < 0)
    throw "Negative Numbers not currently supported!"

  while (n) {
    stack.unshift (n % 2)
    n >>= 1
  }

  return stack
}

/**
 * Multiplies operands using the RUSSE algorithm. Only the integer part
 * of the operands are taken into account.
 *
 * @param {number} op1 First value to multiply.
 * @param {number} op2 Second value to multiply.
 * @param {number} modulo Optional. Set modulo for modulo arithmetic
 *
 * @return op1 * op 2
 *
 * @customfunction
 */

function RUSSE(operand1, operand2, modulo) {

  if (operand1 < 0)
    throw "Operand 1 must be an integer!!"

  if (operand2 < 0)
    throw "Operand 2 must be an integer!!"

  let i_operand1 = Math.abs(operand1)
  let i_operand2 = Math.abs(operand2)

  let result = 0

  while (i_operand2) {
    if (i_operand2 % 2)
      result += i_operand1
    if (modulo)
      result = prune(result, modulo)
    i_operand1 <<= 1
    i_operand2 >>= 1
  }

  return result

}

/**
 * Power By Square and Multiply Modulo.
 *
 * @param {number} base Value to be multiplied by itself.
 * @param {number} exp Exponent.
 * @param {number} modulo Optional. Set modulo for modulo arithmetic
 *
 * @return op1 * op 2
 *
 * @customfunction
 */
function PBSMM (base, exp, modulo) {

  if (!exp || exp < 0)
    throw "Only positive exponents are handled !"

  let binary_exp = binary_stack(exp)
  let result = 1

  while (binary_exp.length) {
    result = RUSSE (result, result, modulo)
    if (binary_exp.shift ())
      result = RUSSE (result, base, modulo)
  }

  return result
}


