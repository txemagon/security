#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HELP "                                                                \n\
    %1$s -  Convert a given string into its Hexadecimal representation.       \n\
                                                                              \n\
    USAGE: %1$s <string>                                                      \n\
                                                                              \n\
    EXAMPLE:                                                                  \n\
       %1$s hello                                                             \n\
       48656C6C6F                                                             \n\
"


const char * PROGNAME;

void
usage (FILE *out, const char *mssg) {
    fprintf (out, "\x1B[31m" "%s" "\x1B[00m" "\n", mssg);
    fprintf (out, "\x1B[36m");
    fprintf (out, HELP "\x1B[00m" "\n", PROGNAME);

    if (out == stderr)
	exit (1);
}


int
main (int argc, char *argv[]) {

    bool filter = false;
    size_t len;
    char **dir_progname = (char **) &PROGNAME;
    char *input = NULL;

    *dir_progname = argv[0];

    if (argc < 2 ) {
        filter = true;
        scanf ("%m[^\n]", &input);
    } else
        input = argv[1];

    len = strlen( input );
    for (int i=0; i<len; i++)
	printf ("%X", (unsigned char) input[i]);

    printf ("\n");

    if (filter)
        free (input);

    return EXIT_SUCCESS;
}

