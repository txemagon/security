#!/bin/bash
#
# gcd - Greatest Common Divisor
#
# Euclidean Algorithm to find the maximal common divisor
# txemagon - 2023


function usage() {
    echo -e "\x1B[36m"
    cat <<HELP

    Greatest Common Divisor

    Usage:
        $0 <number1> <number2>


HELP
    echo -e "\x1B[00m"
}


# Check Number of parameters
if [[ $# -lt 2 ]]; then
    echo "Not enough parameters !" >&2
    usage
    exit 1
fi

# Compute another gcd in the equivalence class
function euclidean_eq() {
    bigger=$1
    lower=$2

    echo $lower $(( $bigger % $lower ))
}


function sort() {
    bigger=$1
    lower=$2

    if [[ $bigger -lt $lower ]]; then
        aux=$bigger
        bigger=$lower
        lower=$aux
    fi

    echo $bigger $lower
}

eq=( $(sort $1 $2) )
while [[ ${eq[1]} -gt 0  ]]; do
    eq=( $( euclidean_eq ${eq[0]} ${eq[1]}) )
done

echo ${eq[0]}

