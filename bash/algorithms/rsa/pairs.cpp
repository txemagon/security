#include <stdio.h>
#include <stdlib.h>


/**
 * Example: ./pairs 120
 *
 * Given a totient it figures out valid key pairs.
 */

int
main (int argc, char *argv[] ) {

    unsigned totient = atoi(argv[1]);

    for (unsigned p1=2; p1<totient / 2; p1++)
	for (unsigned p2=p1+1; p2<totient; p2++)
	    if ( (p1*p2) % totient == 1)
		printf ("%u %u\n", p1, p2);

    return EXIT_SUCCESS;
}
