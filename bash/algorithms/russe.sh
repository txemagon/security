#!/bin/bash
#
# russe - Russian multiplication, ancient egyptian multiplication
# Performs modulo remainder if given
#
# ES7 port
# txemagon - Jan 2023




################################################################################
#  Multiplies operands using the RUSSE algorithm. Only the integer part
#  of the operands are taken into account.
#
#  @param {number} op1 First value to multiply.
#  @param {number} op2 Second value to multiply.
#  @param {number} modulo Optional. Set modulo for modulo arithmetic
#
#  @return op1 * op 2
#
#  @customfunction
#
function RUSSE() {
  operand1=$1
  operand2=$2
  modulo=$3

  if [[ $operand1 -lt 0 ]]; then
    echo "Operand 1 must be an integer!!" >&2
    exit 4
  fi

  if [[ $operand2 -lt 0 ]]; then
    echo "Operand 2 must be an integer!!" >&2
    exit 4
  fi

  i_operand1=$(echo ${operand1#-}) # Math.abs(operand1)
  i_operand2=$(echo ${operand2#-}) # Math.abs(operand2)

  result=0

  while [[ $i_operand2 > 0 ]]; do
      if [[ $(($i_operand2 % 2)) -gt 0 ]]; then
          result=$(($result + $i_operand1))
      fi
    if [ ! -z "$modulo" ]; then
        result=$(( $result % $modulo))
    fi
    i_operand1=$((i_operand1 << 1))
    i_operand2=$((i_operand2 >> 1))
  done

  echo $result

}

