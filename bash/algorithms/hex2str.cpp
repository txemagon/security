#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HELP "                                                                \n\
    %1$s -  Convert a given Hexadecimal number into a string  representation. \n\
                                                                              \n\
    USAGE: %1$s <hex>                                                         \n\
                                                                              \n\
    EXAMPLE:                                                                  \n\
       %1$s 48656C6C6F                                                        \n\
       Hello                                                                  \n\
"


const char * PROGNAME;


void
usage (FILE *out, const char *mssg) {
    fprintf (out, "\x1B[31m" "%s" "\x1B[00m" "\n", mssg);
    fprintf (out, "\x1B[36m");
    fprintf (out, HELP "\x1B[00m" "\n", PROGNAME);

    if (out == stderr)
	exit (1);
}

int
main (int argc, char *argv[]) {

    bool filter = false;
    size_t len;
    unsigned dig;
    char code[2];
    unsigned byte_out;
    char *str_out = NULL;
    char *input = NULL;

    char **dir_progname = (char **) &PROGNAME;

    *dir_progname = argv[0];

    if (argc < 2) {
	// usage (stderr, "Invalid Number of arguments !");
        filter = true;
        scanf ("%ms", &input);
    } else
        input = argv[1];

    len = strlen (input);
    if (len % 2 != 0)
	usage (stderr, "Hex numbers must come by pairs !");

    str_out = (char *) malloc (len/2 + 1);
    code[1] = '\0';
    str_out[len/2] = '\0';

    for (int i=0; i<len; i+=2) {
	sscanf (&input[i], "%2x", &dig);
	sprintf(code, "%c", dig);
	strcat(str_out, code);
    }

    printf ("%s\n", str_out);

    free (str_out);

    if (filter)
        free (input);

    return EXIT_SUCCESS;
}

