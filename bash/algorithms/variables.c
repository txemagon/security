// A short extract from variables.c of BASH
// http://ftp.gnu.org/gnu/bash/bash-5.2.tar.gz

/* Functions for $RANDOM and $SRANDOM */

int last_random_value;
static int seeded_subshell = 0;

static SHELL_VAR *
assign_random (self, value, unused, key)
     SHELL_VAR *self;
     char *value;
     arrayind_t unused;
     char *key;
{
  intmax_t seedval;
  int expok;

  if (integer_p (self))
    seedval = evalexp (value, 0, &expok);
  else
    expok = legal_number (value, &seedval);
  if (expok == 0)
    return (self);
  sbrand (seedval);
  if (subshell_environment)
    seeded_subshell = getpid ();
  return (set_int_value (self, seedval, integer_p (self) != 0));
}

int
get_random_number ()
{
  int rv, pid;

  /* Reset for command and process substitution. */
  pid = getpid ();
  if (subshell_environment && seeded_subshell != pid)
    {
      seedrand ();
      seeded_subshell = pid;
    }

  do
    rv = brand ();
  while (rv == last_random_value);

  return (last_random_value = rv);
}

