#!/bin/bash

OUTFORMAT=0

VALID_ARGS=$(getopt -o aHc:h --long ascii,hex,cypher:,help -- "$@")


usage () {
    cat << EOF

OPTIONS

    -a, --ascii 	Convert HEX string to ASCII
    -H, --hex		Convert ASCII to HEX
    -c, --cypher        Cyphering algorithm:
                          xor:      expects hexadecimal key
                          timesmod: expects hexadecimal key,modulus
    -h, --help 		Show this message

EOF
}



    eval set -- "$VALID_ARGS"

    while [ : ]; do
	case "$1" in -a | --ascii)
                OUTFORMAT=$(($OUTFORMAT + 1))
		shift
		;;
	    -H | --hex)
		OUTFORMAT=$(($OUTFORMAT + 2))
		shift
		;;
	    -h | --help)
		usage
		exit 0
		;;
            -c | --cypher)
                CYPHERALG=$2
                VALIDATEALG=1
                for i in $VALID_ALGS; do
                    [[ $2 == "$i" ]] && VALIDATEALG=0
                done

                [[ $VALIDATEALG -ne 0 ]] && echo -e "\x1B[33mInvalid Algorithm: [$2]\x1B[00m" >&2 && exit 1

                shift 2
                ;;
	    --) shift;
		break
		;;
	esac
    done

    shift $(($OPTIND - 1))
