# MSSG, PASS, CY_COUNT existing variables

source './cypher.sh'

# GLOBAL VARIABLES
STATUS=0
CYPHERING=-1
CY_COUNT=-1


PHASES=("Enter Password" "Enter Text" "Cypher" "Exit")

# LOCAL VARIABLES
local INPUT

enter_state () {

    [[ ${STATUS} -eq 2 ]] && CYPHERING=-1 && OUTPUT=""

}

state_forward () {
    local -n nSTATUS=$1

    STATUS=$(( ${STATUS} + 1 ))
    enter_state "S{STATUS}"
}

state_backward () {

    STATUS=$(( ${STATUS} - 1 ))
    enter_state "S{nSTATUS}"
}


# Receives KEY and alters INPUT
filter_input () {
    local KEY=$1
    local INPUT=$2
    local FOUTPUT=$2

    [[ ! -z "${KEY}" ]] && {

        # Filter Input
        case "${KEY}" in
    	 [[:graph:]]|\ )
    	    FOUTPUT="${INPUT}${KEY}"
    	    ;;
          $'\x7f')
              [[ ${#INPUT} -gt 0 ]] && FOUTPUT="${INPUT:0:-1}"
              ;;
          [[:cntrl:]])
              [[ ! $KEY == $'\x7f' ]] && FOUTPUT="${INPUT}${KEY}"
              ;;

    	 *)
            : #echo -n $KEY | xxd -p >&2
            ;;
        esac

    }

    # echo -n "$KEY"  | xxd -p >&2
    echo -n "${FOUTPUT}"

}

update_state () {
    local KEY=$1

    [[ ! -z "${KEY}" ]] && {

        # Control keys
        case "${KEY}" in
            $'\e[A')
              state_backward STATUS
              ;;
          $'\e[B')
              state_forward STATUS
              ;;

    	 *)
            : # echo -n $KEY | xxd -p >&2
            ;;
        esac

    }


}

state_manager () {
    ###########
    # Get Input

    local KEY=$(read_key)

    case "${STATUS}" in
	0)
            PASS="$(filter_input "${KEY}" "${PASS}")"
	    ;;

	1)
            MSSG="$(filter_input "${KEY}" "${MSSG}")"
	    ;;
	2)
            cypher "${MSSG}" "${PASS}"
            [[ "${CYPHERING}" -ne 0 ]] && CY_COUNT=-1
	    ;;
        3)
            reset
            exit 0
            ;;
	*)
	    exit 1
	    ;;
    esac

    update_state "$KEY"
    bsc_repaint

}


