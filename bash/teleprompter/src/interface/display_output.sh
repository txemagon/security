display_output () {
    local OUTPUT=$1
    local CY_COUNT=$2
    local OUT_CY=$3
    local MSSG=$4
    local PASS=$5

    move_up
    window "Output" "blue" "${LW}%"
    append "${OUTPUT}"
    endwin

    move_up

    col_right
    move_up
    window "OPERATION" "blue" "${RW}%"
    append "XOR:" "left"
    [[ ${CY_COUNT} -ge 0 && ${CY_COUNT} -lt ${#MSSG} ]] && {
        local CHAR=${MSSG:${CY_COUNT}:2}
        local CK=${PASS:$(( ${CY_COUNT} % ${#PASS} )):2}
        append "${CHAR} XOR ${CK} = ${OUT_CY}" "left"
    }
    endwin
}
