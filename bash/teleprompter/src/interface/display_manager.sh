display_manager () {
    local PHASES=("$@")
    local PEN
    local PAPER



    window "TELEPROMPTER STEPS" "blue" "${RW}%"
    for i in ${!PHASES[@]}; do
        PEN="white"
        PAPER="black"

        [[ $i -eq ${STATUS} ]] && {
            PEN="white"
            PAPER="blue"

        }
        append "$((i + 1)). ${PHASES[$i]}" "left" "${PEN}" "${PAPER}"
    done
    endwin

    move_up
}
