# Display splitted and altogether versions of password
# $1: Password String
# $2: Position to be outlined
display_password () {
    local PASS="${1}"
    local POS=$2


    local SPASS="$(select_background "${PASS}" split)"
    local UPASS="$(echo -n "${PASS}" | hexdump -v -e '/1 "%02X"' )"

    if [[ "$CYPHERING" -eq 0 ]]; then
        UPASS="$(select_background "${UPASS}" split2  "$POS" "$(($POS + 1))")"
    else
        UPASS="$(select_background "${UPASS}" split2 ${POS} ${POS})"
    fi

    window "PASSWORD" "blue" "${LW}%"
    append "Password:" "left"
    append "${SPASS}" "left"
    append "${UPASS}" "left"
    endwin

    move_up
    col_right
    window "PASSWORD" "blue" "${RW}%"
    append "${PASS}"
    endwin

}

# Display text splitted by chars.
# $1: Text String
# $2: Position to be outlined
display_text () {
    local MESSAGE="${1}"
    local POS=$2

    local SMESSAGE="$(select_background "${MESSAGE}" split)"

    local UMSSG="$(echo -n "${MESSAGE}" | hexdump -v -e '/1 "%02X"' )"

    if [[ "$CYPHERING" -eq 0 ]]; then
        UMSSG="$(select_background "${UMSSG}" split2 "$POS" "$(($POS + 1))")"
    else
       UMSSG="$(select_background "${UMSSG}" split2 ${POS} ${POS})"
    fi

    move_up
    window "PLAIN TEXT" "blue" "$(( ${LW} + ${RW}))%"
    append "Plain Text:" "left"
    append "${SMESSAGE}" "left"
    append "${UMSSG}" "left"
    endwin
}

# Show password and text
# $1: Input Text
# $2: Password
# $3: Position to be outlined in text
# $4: Position to be outlined in password
display_input () {
    local MSSG="${1}"
    local PASS="${2}"
    local SIMS=$3
    local SIPS=$4

    display_password "${PASS}" "${SIPS}"
    display_text "${MSSG}" "${SIMS}"
}


