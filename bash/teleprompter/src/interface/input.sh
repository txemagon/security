INPUT_DELAY=0.04

read_key() {

    # get pressed key for one second.
    # - if the key is a letter, return the escape sequence
    # - if the is other than a letter, return the escape sequence

    local RET  # The key to be returned
    local D    # Delimiter
    local NI   # No input
    local SCAN # ScanCode
    local success

    read -rsN1 -t ${INPUT_DELAY} RET || exit $?
    success=$?

    if [ "$success" -lt 128 ]; then
        if [ "$RET" == $'\e' ];then
            while IFS='' read -srN1 -t 0.0001 SCAN; do
                RET+="${SCAN}"
            done
            success=$?
        fi
    fi

    read -t 0.0001 -rsd $'\0' D

    [[ $RET == $'\n' ]] && RET=$'\r'

    success=$?

    echo -n "${RET}${D}"

}


