DIR=$( dirname ${BASH_SOURCE[0]})

# CONSTANTS
RW=20
LW=43

source "${DIR}/display_manager.sh"
source "${DIR}/display_input.sh"
source "${DIR}/display_output.sh"
source "${DIR}/../helpers/string.sh"



# Make a visual selection
# $1: Message
# $2: Split text if param 2
#    ==split.  Split text every char.
#    ==split2. Every two chars.
# $3: Start Position
# $4: End Position -included-.
select_background () {
    local STR_MSSG="$(sanitize "$1")"
    local STR_LEN="$(echo -n "${STR_MSSG}" | wc -c)"
    local STR_INIT=${3:-${STR_LEN}}
    local STR_END=${4:-${STR_INIT}}
    local SPLIT_FN="echo -n"

    local LEN

    [[ "$2" =~ split ]]  && {
        SPLIT_FN="split_text"
        LEN=${2:5}
        LEN=${LEN:-1}
    }

    [[ ${STR_INIT} -gt "${STR_LEN}" ]] && echo -n "$(${SPLIT_FN} "${STR_MSSG}" ${LEN})" && return
    echo -n "$($SPLIT_FN "${STR_MSSG:0:${STR_INIT}}" ${LEN})"

    [[ ${STR_END} -gt "${STR_LEN}" ]] && STR_END=$((${STR_LEN} - 1))

    if [[ $STR_END -ge $STR_INIT ]]; then
        echo -ne "\033[01;44m\033[01;37m"
        local SEL="$($SPLIT_FN "${STR_MSSG:${STR_INIT}:$(( ${STR_END} - ${STR_INIT} + 1))}" ${LEN})"
        [[ ${#SEL} -gt 1 ]] && SEL="${SEL:0:-1}"
        echo -n "${SEL}"
        echo -ne "\033[01;49m "

        echo -n "$($SPLIT_FN "${STR_MSSG:$(( ${STR_END} + 1))}" ${LEN})"
    fi
}

refresh_screen () {
    local MSSG="$1"
    local PASS="$2"
    local OUTPUT="$3"
    local CY_COUNT="$4"
    local STATUS="$5"
    local PHASES=("${@:6}")
    local SP=$(($CY_COUNT - 2))

    display_manager "${PHASES[@]}"

    if [[ "${CY_COUNT}" -lt "0" || "${CY_COUNT}" -ge "${#HEXM}" ]]; then
        display_input "${MSSG}" "${PASS}"
    else
        display_input "${MSSG}" "${PASS}" "${SP}" "$((${SP} % ${#HEXP}))"
    fi

    display_output "$OUTPUT" "$SP" "$OUT_CY" "$HEXM" "$HEXP"
}
