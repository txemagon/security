
cypher () {
    local MSSG="$1"
    local PASS="$2"

    HEXM="$(str2hex "${MSSG}")"
    HEXP="$(str2hex "${PASS}")"

    local HEXML="$(echo -n "${HEXM}" | wc -c)"
    local HEXPL="$(echo -n "${HEXP}" | wc -c)"


    [[ $CYPHERING -lt 0 && "${CY_COUNT}" -lt 0 ]] && CY_COUNT=0 && CYPHERING=0

    if [[ $CYPHERING -eq 0 && "${CY_COUNT}" -lt "${HEXML}" ]]; then
        # Convert to HEX
        local CHAR="${HEXM:${CY_COUNT}:2}"
        local CK="${HEXP:$(( ${CY_COUNT} % ${HEXPL} )):2}"

        # Cypher
        OUT_CY="$(xor_cypher "${CHAR}" "${CK}")"

        # Append Output
	OUTPUT="${OUTPUT}${OUT_CY}"

        CY_COUNT=$((${CY_COUNT} + 2))
        # Wait & Paint
        bsc_repaint
        sleep 1
    else
        CYPHERING=1
    fi
}

