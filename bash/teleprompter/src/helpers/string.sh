# Single byte conversion to hex
hexchar () {
    local CHAR=$(printf "%02X" "'$1")
    echo -n "${CHAR}"
}

# String to Hex
str2hex () {
    local MSSG="$1"
    echo -n "${MSSG}" | hexdump -v -e '/1 "%02X"'
}

sanitize () {
    local TXT="$1"

    echo -en "${TXT}" | tr [:cntrl:] ' '
}

split_text () {
    local MSSG="${1}"
    local LEN="${2:-1}"
    local SEP="${3:-.}"

    local AUX=""
    local i

    [[ $LEN -eq 1 ]] &&
    while read -N${LEN} CHAR; do
        # Unicode Character
        for ((i=0; i<$(echo -n "${CHAR}" | wc -c)-1; ++i));do
            AUX="${AUX},, "
        done

        # Not control character
        if [[ "${CHAR}" < " "  ]]; then
            #Control character
            case "${CHAR}" in
                "\x00")
                    AUX=""
                    ;;
                "\x09")
                    AUX="${AUX}\\t "
                    ;;
                "\x0A")
                    AUX="${AUX}\\n "
                    ;;
                "\x0D")
                    AUX="${AUX}\\r "
                    ;;
                #*)
                #    AUX="${AUX}$(printf "%02X" "'$CHAR") "
                #    ;;
            esac
        else
            local AUXSEP="${SEP}"
            #AUX="${AUX}${AUXSEP}-${CHAR}- "
            [[ "${LEN}" -lt 2 ]] && AUX="${AUX}${AUXSEP}"
            AUX="${AUX}${CHAR} "
        fi

    done <<< "${MSSG}"

    [[ "${LEN}" -ne 1 ]] && {
        AUX="$(echo -n "${MSSG}" | sed 's/.\{'${LEN}'\}/& /g')"
    }
    echo -n "${AUX}"
}
