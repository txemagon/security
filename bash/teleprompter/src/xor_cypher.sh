xor_cypher () {

        local CHAR=$1
        local CK=$2

        printf "%02X " "$(( 16#${CHAR} ^ 16#${CK} ))"
}

